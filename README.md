# ActivityPub for Drupal

Implements the ActivityPub protocol for your site. Readers will be able to
follow content on Mastodon and other federated platforms that support
ActivityPub. Responses are possible too (Reply, Like, Announce) with more to
come.

The module has been tested with the following federated platforms:

- Mastodon: follow/accept, post articles/notes (with image), reply, DM
- Pleroma: follow/accept, post articles/notes (with image), reply
- Pixelfed: follow/accept, post photos, reply
- Write as: follow/accept

Open an issue if you have successfully interacted with another platform!

## Features

- Enable ActivityPub per user with summary and type
- Block domains from posting to inbox per user
- Discovery via Webfinger module
- Outbox, Inbox and followers endpoints
- HttpSignature for authorization
- Accept follow requests, Undo (follow)
- Follow (remote) users
- Delete requests when entities are deleted
- Send outbox requests via drush or cron
- Use image styles on avatars and attachments
- Create comments from Create activities from (remote) users
- Caching of image files with the Imagecache external module
- Map Activity types and properties to content types and comments and create
  posts to send (updates) out to the Fediverse.
- Integration with the Scheduler module: https://www.drupal.org/project/scheduler
- Integration with the Personal reader module: https://www.drupal.org/project/reader

## Installation and configuration.

Use composer require drupal/activitypub to get all dependencies:

- Drupal webfinger module
- Drupal nodeinfo module
- landrok/activitypub library

1. Enable Activitypub module
2. Configure permissions:
    - 'View user information' for anonymous users
    - 'Allow users to enable ActivityPub for their account' for authenticated
       user (or a role)
3. At this point, when a user has enabled activitypub at user
/x/activitypub/settings, they should be discoverable. Try searching for
@handle@domain on Mastodon. If you follow this user, an entry will arrive
in the inbox and an Accept outbox entry will be created automatically.
4. Configure global settings at admin/config/services/activitypub to
select how to process those outbox activities. Then either run cron,
configure a crontab for the drush command or manually call it. The
outbox activities will then be processed.
5. On admin/config/services/activitypub/activitypub-type you get an overview
of all ActivityPub Type configuration entities. Five are enabled (and locked)
when enabling this module, which are the 'Accept', 'Follow', 'Delete', 'Undo'
and 'Inbox reply' types. A "Note" example is also installed with basic
configuration but is not enabled yet since this depends on your setup.
6. Create your own configuration
    - Select 'Type plugin'. Public message is the plugin that allows mapping
      content fields with AP properties.
    - Select the content type which you want to map
    - Select the activity (See footnotes underneath)
    - Select the object (use Note if you do not want to populate a title)
    - Map properties to the content type fields.
7. When saved, go to the content type you selected. You can now select the
entity which will process this node in the 'ActivityPub outbox' fieldset.

**Important**

1. The 'Outbox' fieldset has a textarea to enter canonical URLs users in case
they do not follow you.
2. Make sure a Reply, Like or Announce URL point to the id of the post. You can
get this id from the application/activity+json representation of a post.
Examples:
    - Mastodon reply/like
        - Will work: https://mastodon.social/@swentel/104960564425633436
        - Will not work: https://mastodon.social/web/statuses/104960564425633436
    - Pleroma like 'object' needs to be the id of an activity, not the browser
      URL, but works fine for a reply.

Footnotes:

1. Use Create when you want to send regular posts (e.g note, article or reply)
   When you update a post, you can also trigger an Update activity.
2. 'Announce' usually stands for the Boost/Retweet/Repeat response

## Configuring comments

When an Activity is received and has an inReplyTo property, it is possible to
create a comment if the target entity has comments enabled.

You have to create an entity reference field on your comment type which points
to an activity. The activity will be saved, the author and body will be saved
on the comment entity. The reference field does not render anything, so you can
hide it on the  display of the comment. The entity reference field will only be
editable by the owner of the activity or a user who can manage comments.

To enable this feature, go to admin/config/services/activitypub/activitypub-type
and enable the comment plugin that this module ships with. On the configuration
screen of the plugin, you can set the fields and comment format.

To allow users to manage comments on their own content, you can install the
"Manage comments on own content".

See https://www.drupal.org/project/manage_comment_own_content

Note: this only works for nodes at the moment.

## Plugins

ActivityPub types (activities, objects, etc) are managed by plugins. This
module ships with four types of plugins:

- Static types: manages the 'Accept', 'Follow' and 'Undo' activity type.
- Public message: map content types and properties.
- Context: can fetch context for Reply, Like and Announce.
- Comment: handles creating comments on incoming activities.

**Important**: as this module is still alpha, the interface and methods will
most likely change, so be careful when implementing your own plugin. If a
change happens, the release notes of a new release will document those changes.

## Public and Private keys

Public and private keys are saved in private://activitypub/keys.
The default path can be overridden in settings.php via settings:

```
$settings['activitypub_keys_path'] = '/your/path/';
```

## Caching

For incoming or outgoing requests, information needs to be fetched to get the
inbox endpoint for example. This is saved in private://activitypub/cache
The default path and ttl can be overridden in settings.php:

```
$settings['activitypub_cache_path'] = '/your/path/';
$settings['activitypub_cache_ttl'] = 3600;
```

## Default avatar

The default avatar path can be configured on the settings screen. The default
is assets/avatar.png.

## Inbox and outbox

Every user has an inbox and outbox where activities from remote users are
stored and outgoing posts to your followers, unless it's an unlisted or private
post.

'Accept', 'Undo' and handled by the 'Static types' plugin. All other activity
types will be stored, but do not have any impact.

An overview can be found at user/x/activitypub.
A default view which exposes a block is available, but disabled by default.
If enabled, this block will be used on user/x/activitypub with default exposed
filters.

## Signature verification

Incoming activities are verified, however, not all signatures might be valid
yet, depending on the source. In case the activity is a post, and you are
following the user (or it's a reply against a post from a user you follow), the
status will also be set to published.

In the meantime, we're trying to make sure all variations of signing are
implemented. Help is always welcome :)

## Sending posts to the Fediverse

On nodes a fieldset will be available to send it to the Fediverse.
Activities in the outbox are stored in a queue and send either by cron or
drush. Configure this at /admin/config/services/activitypub

This happens in two steps:

First preparing: checks all (shared) inboxes and created individual queue items
for every (shared) inbox.

The drush command is activitypub:prepare-outbox and has 4 parameters:

- create: create send queue item (defaults to 1)
- time_limit: how long the process may run (defaults to 30 seconds)
- delete: remove queue item (defaults to 1)
- debug: show command line debug messages (defaults to 0)

In a second step, the prepared requests are send out.
The drush command is activitypub:process-outbox and has 4 parameters:

- send: send request (defaults to 1)
- time_limit: how long the process may run (defaults to 30 seconds)
- delete: remove queue item (defaults to 1)
- debug: show command line debug messages (defaults to 0)

## Processing inbox activities

A Create activity with the inReplyTo property will be added to the queue to try
and create a comment on the entity which is stored in the inReplyTo property.
Note that the comment plugin for this needs to be enabled.

The drush command is activitypub:process-inbox and has 3 parameters:

- time_limit: how long the process may run (defaults to 30 seconds)
- delete: remove queue item (defaults to 1)
- debug: show command line debug messages (defaults to 0)

## Auto deleting old activities

You can remove old activities after a certain period. The configuration of the
days is at admin/config/services/activitypub.

The drush command is activitypub:remove-old-activities

## Drush commands

- activitypub:prepare-outbox: create queue items per (shared) inbox for activities.
- activitypub:process-outbox: process activities ready to be send out, created by prepare-outbox.
- activitypub:process-inbox: process activities (e.g. Create with reply)
- activitypub:add-to-queue: adds an activity to the queue
- activitypub:delete-activity: send a delete request
- activitypub:webfinger-info: test command to get info about a remote user

## FAQ

### Can I follow users

You can by creating a new Follow activity with following properties:

- type "Follow"
- collection: "outbox"
- object: url of the user to follow
- actor: your actor url
- config id: follow
- status: unpublished

Leave the 'to' textarea empty, the object will be used to send to the users'
inbox. These properties are automatically filled populated when using the
"Follow user" action, which also hides all form elements, except the user url
textfield.

After saving, a queue item will be generated if all parameters are ok. If not,
use the "Add to queue" operation for this activity.

The "Accept" request sent back by the user will set the status of the follow
activity to 1. To unfollow the user, use the 'Undo' operation on this activity.

### What happens if a local post is deleted

When you delete a post (node, comment) which has been sent to (remote) users, a
"Delete" activity will be created in the outbox collection to notify the other
platforms to remove this post.

### What is the 'Move' activity in my Inbox collection

Mastodon has a feature where accounts can be moved to another instance. When
this activity is stored in the Inbox, two things might happen:

- A new outbox 'Follow' request will be created in case you follow this user.
- The original inbox Follow and Accept requests from/to this user are deleted.

Other activities from the original instance will not be removed.

### Can I send DM's

You can: set the post privacy (visibility) to Private.
Also, when configuring a content type, set the default status to unpublished!

This is alpha, so be careful, and simply don't send important information via
direct messages!

### What does 'Mute' do in the Reader?

This will set the 'Follow' activity to unpublished so that messages of this
user will not appear anymore in the home timeline. They will show up in the
local timeline. To unmute a user, go to 'Sources', then 'Fediverse' or find
a post from the user in the Local timeline.

### Can I use this together with the IndieWeb module

You can use the Microsub module (bundled in the IndieWeb module) to follow
users and then use your favorite reader client to read content and interact via
Micropub.

If you are using a Micropub client, using one or more syndication targets
can help you to create an outbox request. Create a syndication target which
is not a URL. These won't be added then to the webmention queue. Once done,
you need to implement two hooks with custom code:

```
/**
 * Implements hook_indieweb_micropub_node_pre_create_alter().
 */
function hook_indieweb_micropub_node_pre_create_alter(&$values, &$payload) {
  // Reset mp-syndicate-to in case you configure to automatically send
  // a webmention to the link found in in-reply-to.
  if (!empty($payload['mp-syndicate-to'])) {
    if (in_array('activitypub_reply', $payload['mp-syndicate-to'])) {
      $payload['mp-syndicate-to'] = ['activitypub_reply'];
    }
  }
}

/**
 * Implements hook_indieweb_micropub_node_saved().
 */
function hook_indieweb_micropub_node_saved(NodeInterface $node, $values, $payload, $payload_original) {
  if (!empty($payload_original['mp-syndicate-to'])) {
    foreach ($payload_original['mp-syndicate-to'] as $target) {
      switch ($target) {
        // Value of syndication target.
        case 'activitypub_reply':
          if ($node->bundle() == 'reply') {

            $values = [
              'status' => TRUE,
              'collection' => 'outbox',
              // id of reply config entity
              'config_id' => 'reply',
              'uid' => $node->getOwnerId(),
              // Replace {actor_name} with the activitypub name.
              'actor' => 'https://yourdomain.com/user/1/activitypub/{actor_name}',
              'entity_type_id' => 'node',
              'entity_id' => $node->id(),
              'processed' => FALSE,
            ];

            // This is a reply, so we have the in-reply-to microformat value.
            // Other values of this can be 'followers' or the canonical link
            // of a user.
            $values['object'] = $payload_original['in-reply-to'][0];

            /** @var \Drupal\activitypub\Entity\ActivityPubActivityInterface $activity */
            $activity = \Drupal::entityTypeManager()->getStorage('activitypub_activity')->create($values);
            $activity->save();
            \Drupal::service('activitypub.outbox.client')->createQueueItem($activity);
            Cache::invalidateTags(['user:' . $node->getOwnerId()]);
          }
          break;
      }
    }
  }
}
```

## Sponsors

I would like to extend many thanks to the following sponsors for funding development.

- [NLnet Foundation](https://nlnet.nl) and [NGI0
Discovery](https://nlnet.nl/discovery/), part of the [Next Generation
Internet](https://ngi.eu) initiative.
