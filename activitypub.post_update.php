<?php

/**
 * @file
 * Post updates for the ActivityPub module.
 */

/**
 * Installs the "Follow" ActivityPub Type.
 */
function activitypub_post_update_001() {
  $values = [
    'id' => 'follow',
    'label' => 'Follow',
    'description' => 'Sends the "Follow" request when a local user wants to follow a (remote) user.',
    'locked' => TRUE,
    'plugin' => [
      'id' => 'activitypub_static_types',
      'configuration' => [
        'target_entity_type_id' => '',
        'target_bundle' => '',
        'activity' => 'Follow',
        'object' => '',
      ],
    ],
  ];
  $type = \Drupal::entityTypeManager()->getStorage('activitypub_type')->create($values);
  $type->save();
}

/**
 * Installs the "Undo" ActivityPub Type.
 */
function activitypub_post_update_002() {
  $values = [
    'id' => 'undo',
    'label' => 'Undo',
    'description' => 'Sends the "Undo" request for a local activity.',
    'locked' => TRUE,
    'plugin' => [
      'id' => 'activitypub_static_types',
      'configuration' => [
        'target_entity_type_id' => '',
        'target_bundle' => '',
        'activity' => 'Undo',
        'object' => '',
      ],
    ],
  ];
  $type = \Drupal::entityTypeManager()->getStorage('activitypub_type')->create($values);
  $type->save();
}

/**
 * Installs the "Context" ActivityPub Type.
 */
function activitypub_post_update_003() {
  $values = [
    'id' => 'context',
    'label' => 'Context',
    'description' => "Fetches context for specific Activities: like, announce and replies.\nWhen enabled, the plugin will set the activity to be queued so the context can be fetched later.",
    'locked' => TRUE,
    'status' => FALSE,
    'plugin' => [
      'id' => 'activitypub_context',
      'configuration' => [],
    ],
  ];
  $type = \Drupal::entityTypeManager()->getStorage('activitypub_type')->create($values);
  $type->save();
}
