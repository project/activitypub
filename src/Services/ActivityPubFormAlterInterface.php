<?php

namespace Drupal\activitypub\Services;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;

interface ActivityPubFormAlterInterface {

  /**
   * Adds the ActivityPub settings form element.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @param $type
   */
  public function addActivityPubSettingsFormElement(array &$form, FormStateInterface $form_state, EntityInterface $entity, $type);

  /**
   * Adds the ActivityPub outbox form element.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param \Drupal\Core\Entity\EntityInterface $entity
   */
  public function addActivityPubOutboxFormElement(array &$form, FormStateInterface $form_state, EntityInterface $entity);

  /**
   * Alter the comment form.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param \Drupal\Core\Entity\EntityInterface $entity
   */
  public function alterCommentForm(array &$form, FormStateInterface $form_state, EntityInterface $entity);

}
