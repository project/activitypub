<?php

namespace Drupal\activitypub\Services;

use Drupal\activitypub\Entity\ActivityPubActivityInterface;
use Drupal\activitypub\Entity\ActivityPubActorInterface;
use Drupal\Core\Entity\EntityInterface;

interface ActivityPubOutboxInterface {

  /**
   * Create outbox activity.
   *
   * @param string $type
   *   The ActivityPub type
   * @param EntityInterface $entity
   *   The entity
   * @param \Drupal\activitypub\Entity\ActivityPubActorInterface $actor
   *   The actor
   * @param int $visibility
   *   The post privacy.
   * @param string $to
   *   Extra send parameter.
   */
  public function createActivity(string $type, EntityInterface $entity, ActivityPubActorInterface $actor, int $visibility = ActivityPubActivityInterface::VISIBILITY_PUBLIC, string $to = '');

  /**
   * Update outbox activity.
   *
   * @param \Drupal\activitypub\Entity\ActivityPubActivityInterface $activity
   */
  public function updateActivity(ActivityPubActivityInterface $activity);

}
