<?php

namespace Drupal\activitypub\Services;

use ActivityPhp\Server;
use Symfony\Component\HttpFoundation\Request;

interface ActivityPubSignatureInterface {

  /**
   * Generate keys in a path.
   *
   * @param $path
   *
   * @return bool
   */
  public function generateKeys($path);

  /**
   * Delete keys.
   *
   * @param $path
   */
  public function deleteKeys($path);

  /**
   * Get the public key for a path.
   *
   * @param $path
   *
   * @return string
   */
  public function getPublicKey($path);

  /**
   * Get the private key for a path.
   *
   * @param $path
   *
   * @return string
   */
  public function getPrivateKey($path);

  /**
   * Create a signature.
   *
   * @param $private_key_path
   * @param $host
   * @param $path
   * @param $digest
   * @param null $date
   *
   * @return string|Null
   */
  public function createSignature($private_key_path, $host, $path, $digest, $date = NULL);

  /**
   * Create a digest.
   *
   * @param $message
   *
   * @return string
   */
  public function createDigest($message);

  /**
   * Verify signature.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @param string $actor
   * @param \ActivityPhp\Server $server
   *
   * @return bool
   */
  public function verifySignature(Request $request, string $actor, Server $server);

}