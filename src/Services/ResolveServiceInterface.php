<?php

namespace Drupal\activitypub\Services;

/**
 * Provides the resolve service.
 */
interface ResolveServiceInterface {

  /**
   * Resolves a uri to valid status or actor.
   *
   * @param string $uri
   *   The uri to resolve as activity or actor.
   *
   * @return array
   *   An array with the resolved accounts and activities.
   */
  public function resolveQuery(string $uri);

}
