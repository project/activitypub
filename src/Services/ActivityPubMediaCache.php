<?php

namespace Drupal\activitypub\Services;

use DOMDocument;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\image\Entity\ImageStyle;

class ActivityPubMediaCache implements ActivityPubMediaCacheInterface {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The file url generator.
   *
   * @var \Drupal\Core\file\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  /**
   * ActivityPubCacheClient constructor
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ModuleHandlerInterface $module_handler, FileUrlGeneratorInterface $file_url_generator) {
    $this->configFactory = $config_factory;
    $this->moduleHandler = $module_handler;
    $this->fileUrlGenerator = $file_url_generator;
  }

  /**
   * {@inheritdoc}
   */
  public function replaceImagesInString($content, $type = 'cache_attachment') {
    $images = [];
    $extracted_images = $this->extractImages($content);
    if (!empty($extracted_images)) {
      foreach ($extracted_images as $image) {
        $images[$image] = $this->applyImageCache($image, $type);
      }
      $content = str_replace(array_keys($images), array_values($images), $content);
    }
    return $content;
  }

  /**
   * {@inheritdoc}
   */
  public function applyImageCache($filename, $type = 'cache_avatar') {
    // Return early if the filename is empty.
    if (empty($filename)) {
      return $filename;
    }

    $config = $this->configFactory->get('activitypub.settings');
    if ($config->get('cache_images') && $this->moduleHandler->moduleExists('imagecache_external')) {
      $filename = imagecache_external_generate_path($filename);

      // Apply image style.
      $image_style = $config->get($type . '_style');
        $style = ImageStyle::load($image_style);
        if ($style) {
          if ($style->supportsUri($filename)) {
            $filename = $style->buildUrl($filename);
        }
        else {
          $filename = $this->fileUrlGenerator->generateAbsoluteString($filename);
        }
      }
      else {
        $filename = $this->fileUrlGenerator->generateAbsoluteString($filename);
      }
    }

    return $filename;
  }

  /**
   * Extracts images from a string.
   *
   * @param $html
   *
   * @return array $images
   */
  protected function extractImages($html) {
    $images = [];

    $dom = new domDocument;
    libxml_use_internal_errors(TRUE);
    $dom->loadHTML($html);
    $dom->preserveWhiteSpace = FALSE;
    $image_list = $dom->getElementsByTagName('img');

    foreach ($image_list as $image) {
      $images[] = $image->getAttribute('src');
    }

    return $images;
  }

}
