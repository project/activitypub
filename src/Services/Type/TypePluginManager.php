<?php

namespace Drupal\activitypub\Services\Type;

use Drupal\activitypub\Annotation\ActivityPubType;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Defines a plugin manager for ActivityPub objects.
 */
class TypePluginManager extends DefaultPluginManager {

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/activitypub/type', $namespaces, $module_handler, TypePluginInterface::class, ActivityPubType::class);
    $this->alterInfo('activitypub_type_plugin_info');
  }

  /**
   * {@inheritdoc}
   */
  protected function findDefinitions() {
    $definitions = parent::findDefinitions();
    uasort($definitions, array('Drupal\Component\Utility\SortArray', 'sortByWeightElement'));
    return $definitions;
  }


}
