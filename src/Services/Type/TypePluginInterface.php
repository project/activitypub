<?php

namespace Drupal\activitypub\Services\Type;

use Drupal\activitypub\Entity\ActivityPubActivityInterface;
use Drupal\activitypub\Entity\ActivityPubTypeInterface;
use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides an interface describing the activitypub types.
 */
interface TypePluginInterface extends ConfigurableInterface, DependentPluginInterface {

  /**
   * Returns whether to expose in type configuration form.
   *
   * @return bool
   */
  public function isExposed();

  /**
   * Returns the activities managed by this plugin.
   *
   * @return array
   *   A collection of objects.
   */
  public function getActivities();

  /**
   * Returns the objects managed by this plugin.
   *
   * @return array
   *   A collection of objects.
   */
  public function getObjects();

  /**
   * Returns the supported properties for an object.
   *
   * @param $object
   *
   * @return array
   *   A collection of objects.
   */
  public function getProperties($object);

  /**
   * Act on pre save of an activity arriving in the Inbox.
   *
   * @param \Drupal\activitypub\Entity\ActivityPubActivityInterface $activity
   *   The activity being saved.
   * @param $doSave
   *   Whether to save the activity. Set to FALSE if you don't want the activity
   *   to be saved.
   */
  public function onActivityInboxPreSave(ActivityPubActivityInterface $activity, &$doSave);

  /**
   * Act on outbox pre save.
   *
   * @param \Drupal\activitypub\Entity\ActivityPubActivityInterface $activity
   *   The activity being saved.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity being saved, usually a node.
   * @param \Drupal\activitypub\Entity\ActivityPubTypeInterface $activityPubType
   *   The activitypub type.
   * @param $doSave
   *   Whether to save the activity. Set to FALSE if you don't want the activity
   *   to be saved.
   *
   * @return mixed
   */
  public function onActivityOutboxPreSave(ActivityPubActivityInterface $activity, EntityInterface $entity, ActivityPubTypeInterface $activityPubType, &$doSave);

  /**
   * Act on post save of an activity.
   *
   * @param \Drupal\activitypub\Entity\ActivityPubActivityInterface $activity
   *   The activity which was saved.
   * @param $update
   *   Specifies whether the entity is being updated or created.
   */
  public function onActivityPostSave(ActivityPubActivityInterface $activity, $update = TRUE);

  /**
   * Act when a content entity is deleted, apart from user or Activitypub actor.
   *
   * @param \Drupal\activitypub\Entity\ActivityPubActivityInterface $activity
   *   The activity that references the entity.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity that is being deleted.
   *
   * @return mixed
   */
  public function onEntityDelete(ActivityPubActivityInterface $activity, EntityInterface $entity);

  /**
   * Act when an activity is deleted.
   *
   * @param \Drupal\activitypub\Entity\ActivityPubActivityInterface $activity
   */
  public function onActivityDelete(ActivityPubActivityInterface $activity);

  /**
   * Provides a configuration form for this type.
   *
   * @param array $form
   *   A form API form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return array
   *   A form array.
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state);

  /**
   * Build the activity.
   *
   * @param \Drupal\activitypub\Entity\ActivityPubActivityInterface $activity
   * @param \Drupal\Core\Entity\EntityInterface|null $entity
   *
   * @return array
   */
  public function build(ActivityPubActivityInterface $activity, EntityInterface $entity = NULL);

  /**
   * Do inbox processing.
   *
   * @param \Drupal\activitypub\Entity\ActivityPubActivityInterface $activity
   * @param \Drupal\Core\Entity\EntityInterface|null $entity
   */
  public function doInboxProcess(ActivityPubActivityInterface $activity, EntityInterface $entity = NULL);

}
