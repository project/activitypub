<?php

namespace Drupal\activitypub\Services;

use Drupal\activitypub\Entity\ActivityPubActorInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\user\UserInterface;
use Symfony\Component\HttpFoundation\Request;

interface ActivityPubUtilityInterface {

  /**
   * Returns the ActivityPub name.
   *
   * @param \Drupal\activitypub\Entity\ActivityPubActorInterface $actor
   *
   * @return string
   */
  public function getActivityPubName(ActivityPubActorInterface $actor);

  /**
   * Returns the ActivityPub ID based on the Actor.
   *
   * @param \Drupal\activitypub\Entity\ActivityPubActorInterface $actor
   *
   * @return string
   */
  public function getActivityPubID(ActivityPubActorInterface $actor);

  /**
   * Returns an image from the user, if any.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user.
   * @param string $type
   *   The image type. Either avatar or header.
   *
   * @return string|NULL
   */
  public function getActivityPubUserImage(UserInterface $user, string $type = 'avatar');

  /**
   * Get the ActivityPubServer.
   *
   * @param array $config
   *
   * @return \ActivityPhp\Server
   */
  public function getServer(array $config = []);

  /**
   * Alter the nodeinfo data.
   *
   * @param array $data
   */
  public function alterNodeInfo(array &$data);

  /**
   * Act on entity delete.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *
   * @return mixed
   */
  public function onEntityDelete(EntityInterface $entity);

  /**
   * Get the visibility options.
   *
   * @return array
   */
  public function getVisibilityOptions();

  /**
   * Returns a collection of types to ignore for the outbox.
   *
   * @return array
   */
  public function getOutboxIgnoreTypes();

  /**
   * Returns a collection of types to include for timelines.
   *
   * @return array
   */
  public function getTimelineTypes();

  /**
   * Returns a collection of types to include for notification timelines.
   *
   * @return array
   */
  public function getNotificationTypes();

}
