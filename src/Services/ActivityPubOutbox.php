<?php

namespace Drupal\activitypub\Services;

use Drupal\activitypub\Entity\ActivityPubActivityInterface;
use Drupal\activitypub\Entity\ActivityPubActorInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

class ActivityPubOutbox implements ActivityPubOutboxInterface {

  /**
   * The ActivityPub actor storage.
   *
   * @var \Drupal\activitypub\Entity\Storage\ActivityPubActorStorageInterface
   */
  protected $actorStorage;

  /**
   * The ActivityPub type storage.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface
   */
  protected $typeStorage;

  /**
   * The ActivityPub Activity storage.
   *
   * @var \Drupal\activitypub\Entity\Storage\ActivityPubActivityStorageInterface
   */
  protected $activityStorage;

  /**
   * The ActivityPub utility service.
   *
   * @var \Drupal\activitypub\Services\ActivityPubUtilityInterface
   */
  protected $activityPubUtility;

  /**
   * The process client.
   *
   * @var \Drupal\activitypub\Services\ActivityPubProcessClientInterface
   */
  protected $activityPubProcessClient;

  /**
   * ActivityPubOutbox constructor
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\activitypub\Services\ActivityPubUtilityInterface $activitypub_utility
   *   The ActivityPub utility service.
   * @param \Drupal\activitypub\Services\ActivityPubProcessClientInterface $activitypub_process_client
   *   The ActivityPub process client service.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ActivityPubUtilityInterface $activitypub_utility, ActivityPubProcessClientInterface $activitypub_process_client) {
    $this->actorStorage = $entity_type_manager->getStorage('activitypub_actor');
    $this->typeStorage = $entity_type_manager->getStorage('activitypub_type');
    $this->activityStorage = $entity_type_manager->getStorage('activitypub_activity');
    $this->activityPubUtility = $activitypub_utility;
    $this->activityPubProcessClient = $activitypub_process_client;
  }

  /**
   * {@inheritdoc}
   */
  public function createActivity(string $type, EntityInterface $entity, ActivityPubActorInterface $actor, int $visibility = ActivityPubActivityInterface::VISIBILITY_PUBLIC, string $to = '') {

    /** @var \Drupal\activitypub\Entity\ActivityPubTypeInterface $activityPubType */
    $activityPubType = $this->typeStorage->load($type);
    if (!$activityPubType) {
      return;
    }

    $uid = $actor->getOwnerId();

    $values = [
      'collection' => 'outbox',
      'config_id' => $type,
      'type' => $activityPubType->getPlugin()['configuration']['activity'],
      'uid' => $uid,
      'actor' => $this->activityPubUtility->getActivityPubID($actor),
      'entity_type_id' => $entity->getEntityTypeId(),
      'entity_id' => $entity->id(),
      'processed' => FALSE,
      'status' => TRUE,
      'visibility' => $visibility
    ];

    if ($to) {
      $values['to'] = $to;
    }

    /** @var \Drupal\activitypub\Entity\ActivityPubActivityInterface $activity */
    $activity = $this->activityStorage->create($values);

    $doSave = TRUE;
    $activity->preOutboxSave($entity, $activityPubType, $doSave);
    if ($doSave) {
      $activity->save();
      if ($activity->isPublished()) {
        $this->activityPubProcessClient->createQueueItem($activity);
        Cache::invalidateTags(['user:' . $uid]);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function updateActivity(ActivityPubActivityInterface $activity) {
    if ($activity->isPublished()) {
      $this->activityPubProcessClient->createQueueItem($activity, ACTIVITYPUB_OUTBOX_QUEUE, ['replaceType' => 'Update']);
      Cache::invalidateTags(['user:' . $activity->getOwnerId()]);
    }
  }

}
