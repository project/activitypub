<?php

namespace Drupal\activitypub\Services;

interface ActivityPubMediaCacheInterface {

  /**
   * Replaces images in strings.
   *
   * @param $content
   * @param $type
   *
   * @return string $content
   */
  public function replaceImagesInString($content, $type = 'cache_attachment');

  /**
   * Applies image cache to an image.
   *
   * @param $filename
   *   The filename
   * @param string $type
   *   The type, e.g. avatar_user or attachment_content
   *
   * @return string $filename
   */
  public function applyImageCache($filename, $type = 'cache_avatar');

}
