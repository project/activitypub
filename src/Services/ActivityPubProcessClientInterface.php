<?php

namespace Drupal\activitypub\Services;

use Drupal\activitypub\Entity\ActivityPubActivityInterface;

interface ActivityPubProcessClientInterface {

  /**
   * Prepare the outbox queue.
   *
   * Based on the host of the followers, a queue item will be created per host
   * so that when the actual items are send out, we can more easily catch
   * exceptions (e.g. host not found, timeouts etc.) and that other requests to
   * different hosts don't fail.
   *
   * @param bool $create_send_queue_item
   *   Whether to create send queue items.
   * @param int $time_limit
   *   How long in seconds this method may run.
   * @param bool $remove_item_from_queue
   *   Whether to remove the item from the queue.
   * @param bool $debug
   *   Whether to view debug statements, useful when using Drush.
   *
   * @return mixed
   */
  public function prepareOutboxQueue($create_send_queue_item = TRUE, $time_limit = 30, $remove_item_from_queue = TRUE, $debug = FALSE);

  /**
   * Handles the Outbox queue.
   *
   * @param bool $send
   *   Whether to send to inboxes.
   * @param int $time_limit
   *   How long in seconds this method may run.
   * @param bool $remove_item_from_queue
   *   Whether to remove the item from the queue.
   * @param bool $debug
   *   Whether to view debug statements, useful when using Drush.
   */
  public function handleOutboxQueue($send = TRUE, $time_limit = 30, $remove_item_from_queue = TRUE, $debug = FALSE);

  /**
   * Handles the Inbox queue.
   *
   * @param int $time_limit
   *   How long in seconds this method may run.
   * @param bool $remove_item_from_queue
   *   Whether to remove the item from the queue.
   * @param false $debug
   *   Whether to view debug statements, useful when using Drush.
   */
  public function handleInboxQueue($time_limit = 30, $remove_item_from_queue = TRUE, $debug = FALSE);

  /**
   * Generates a queue item, either inbox or outbox.
   *
   * @param \Drupal\activitypub\Entity\ActivityPubActivityInterface $activityPubActivity
   * @param null $queue
   * @param array $extra_data
   */
  public function createQueueItem(ActivityPubActivityInterface $activityPubActivity, $queue = NULL, array $extra_data = []);

  /**
   * Generates a outbox send queue item.
   *
   * @param \Drupal\activitypub\Entity\ActivityPubActivityInterface $activityPubActivity
   *   The activity.
   * @param $build
   *   The build for this activity.
   * @param $targets
   *   The targets to send to.
   *
   * @return mixed
   */
  public function createSendQueueItem(ActivityPubActivityInterface $activityPubActivity, $build, $targets);

  /**
   * Remove old activities.
   */
  public function removeOldActivities();
}
