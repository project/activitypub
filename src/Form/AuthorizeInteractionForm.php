<?php

namespace Drupal\activitypub\Form;

use Drupal\activitypub\ActivityPubAccessTrait;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\activitypub\Services\ActivityPubUtilityInterface;
use Drupal\activitypub\Entity\ActivityPubActivityInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\activitypub\Services\ResolveServiceInterface;

/**
 * Provides the AuthorizeInteractionForm form.
 */
class AuthorizeInteractionForm extends FormBase {

  use ActivityPubAccessTrait;

  /**
   * The activitypub.utility service.
   *
   * @var \Drupal\activitypub\Services\ActivityPubUtilityInterface
   */
  protected $utility;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Value for current url request.
   *
   * @var string
   *   RequestStack value
   */
  protected $requestStack;

  /**
   * The activitypub resolver.
   *
   * @var \Drupal\activitypub\Services\ResolveServiceInterface
   */
  protected $activitypubResolver;

  /**
   * The constructor.
   *
   * @param \Drupal\activitypub\Services\ActivityPubUtilityInterface $utility
   *   The activitypub.utility service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\activitypub\Services\ResolveServiceInterface $activitypub_resolver
   *   The activitypub resolver.
   */
  public function __construct(ActivityPubUtilityInterface $utility, EntityTypeManagerInterface $entity_type_manager, RequestStack $request_stack, ResolveServiceInterface $activitypub_resolver) {
    $this->utility = $utility;
    $this->entityTypeManager = $entity_type_manager;
    $this->requestStack = $request_stack;
    $this->activitypubResolver = $activitypub_resolver;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('activitypub.utility'),
      $container->get('entity_type.manager'),
      $container->get('request_stack'),
      $container->get('activitypub.resolve_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'activitypub_authorize_interaction';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $uri = $this->requestStack->getCurrentRequest()->query->get('uri');
    $results = $this->activitypubResolver->resolveQuery($uri);

    $form['actions'] = [
      '#type' => 'actions',
    ];

    if (count($results['accounts']) > 0) {
      $form['preview'] = [
        '#theme' => 'activitypub_remote_person',
        '#person' => $results['accounts'][0],
      ];
      $form_state->set('object', $results['accounts'][0]['id']);
      $form['actions']['follow'] = [
        '#type' => 'submit',
        '#value' => $this->t('Follow'),
      ];

    }

    if (count($results['statuses']) > 0) {
      $form['preview'] = [
        '#theme' => 'activitypub_remote_activity',
        '#activity' => $results['statuses'][0],
      ];
      $form['actions']['announce'] = [
        '#type' => 'submit',
        '#value' => $this->t('Announce'),
      ];
      $form_state->set('object', $results['statuses'][0]['id']);

      $form['actions']['like'] = [
        '#type' => 'submit',
        '#value' => $this->t('Like'),
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue('object')) {
      $form_state->setErrorByName('object', $this->t('No object to interact.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\activitypub\Entity\Storage\ActivityPubActorStorageInterface $storage */
    $actor_storage = $this->entityTypeManager->getStorage('activitypub_actor');
    $actor = $actor_storage->loadActorByEntityIdAndType($this->currentUser()->id(), 'person');
    $actor_id = $this->utility->getActivityPubId($actor);

    $action = $form_state->getTriggeringElement()['#value'];
    switch ($action) {
      case 'Follow':
        $values = [
          'type' => 'Follow',
          'collection' => ActivityPubActivityInterface::OUTBOX,
          'config_id' => 'follow',
          'status' => FALSE,
          'object' => $form_state->get('object'),
          'actor' => $actor_id,
        ];
        break;

      case 'Announce':
        $values = [
          'type' => 'Follow',
          'collection' => ActivityPubActivityInterface::OUTBOX,
          'status' => FALSE,
          'object' => $form_state->get('object'),
          'actor' => $actor_id,
        ];
        break;

      case 'Like':
        $values = [
          'type' => 'Like',
          'collection' => ActivityPubActivityInterface::OUTBOX,
          'status' => FALSE,
          'object' => $form_state->get('object'),
          'actor' => $actor_id,
        ];
        break;
    }
    $storage = $this->entityTypeManager->getStorage('activitypub_activity');
    /** @var \Drupal\activitypub\Entity\ActivityPubActivityInterface $follow */
    $activity = $storage->create($values);
    $activity->save();

    $this->messenger()->addStatus($this->t('The activity has been created.'));
    $form_state->setRedirect('<front>');
  }

}
