<?php

namespace Drupal\activitypub\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ActivityPubSettingsForm extends ConfigFormBase {

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The entity field type plugin manager.
   *
   * @var \Drupal\Core\Field\FieldTypePluginManagerInterface
   */
  protected $fieldTypePluginManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * SettingsForm constructor
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The entity field manager.
   * @param \Drupal\Core\Field\FieldTypePluginManagerInterface $fieldTypePluginManager
   *   The entity field type plugin manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typed_config_manager
   *   The typed config manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityFieldManagerInterface $entityFieldManager, FieldTypePluginManagerInterface $fieldTypePluginManager, ModuleHandlerInterface $module_handler, TypedConfigManagerInterface $typed_config_manager) {
    parent::__construct($config_factory, $typed_config_manager);
    $this->entityFieldManager = $entityFieldManager;
    $this->fieldTypePluginManager = $fieldTypePluginManager;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_field.manager'),
      $container->get('plugin.manager.field.field_type'),
      $container->get('module_handler'),
      $container->get('config.typed')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['activitypub.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'activitypub_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('activitypub.settings');

    $form['media'] = [
      '#type' => 'details',
      '#title' => $this->t('Media'),
      '#open' => TRUE
    ];

    $form['media']['avatar_user_style'] = [
      '#title' => $this->t('Avatar image style'),
      '#description' => $this->t('This image style will be used for the avatar for local and remote users.'),
      '#default_value' => $config->get('avatar_user_style'),
      '#type' => 'select',
      '#options' => image_style_options(FALSE),
    ];

    $user_image_fields = ['' => $this->t('- None -')];
    $fields = $this->entityFieldManager->getFieldStorageDefinitions('user');
    $field_types = $this->fieldTypePluginManager->getDefinitions();
    foreach ($fields as $key => $field) {
      if ($field->getType() == 'image' || ($field->getType() == 'entity_reference' && $field->getSetting('target_type') == 'media')) {
        $user_image_fields[$key] = $field_types[$field->getType()]['label'] . ': ' . $field->getName();
      }
    }

    $form['media']['avatar_user_field'] = [
      '#title' => $this->t('Avatar field'),
      '#description' => $this->t('Select the field on the user which contains the user avatar.'),
      '#default_value' => $config->get('avatar_user_field'),
      '#type' => 'select',
      '#options' => $user_image_fields,
    ];

    $form['media']['header_user_field'] = [
      '#title' => $this->t('Header field'),
      '#description' => $this->t('Select the field on the user which contains the header image.'),
      '#default_value' => $config->get('header_user_field'),
      '#type' => 'select',
      '#options' => $user_image_fields,
    ];

    $form['media']['header_user_style'] = [
      '#title' => $this->t('Header image style'),
      '#description' => $this->t('This image style will be used for the header for local and remote users.'),
      '#default_value' => $config->get('header_user_style'),
      '#type' => 'select',
      '#options' => image_style_options(FALSE),
      '#states' => [
        'visible' => [
          ':input[name="header_user_field"]' => ['!value' => ''],
        ]
      ],
    ];

    $form['media']['attachment_content_style'] = [
      '#title' => $this->t('Attachment content image style'),
      '#description' => $this->t('This image style will be used for the image attachments on content.'),
      '#default_value' => $config->get('attachment_content_style'),
      '#type' => 'select',
      '#options' => image_style_options(),
    ];

    $form['media']['avatar_default_path'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Default avatar path'),
      '#description' => $this->t('The default avatar in case the user has not uploaded a picture. The default is "assets/avatar.png"'),
      '#default_value' => $config->get('avatar_default_path'),
    ];

    $form['media']['cache_images'] = [
      '#title' => $this->t('Cache images'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('cache_images'),
      '#description' => $this->t('Cache avatars and photos in the Inbox locally using the imagecache external module.<br />Used by the Personal reader.'),
      '#disabled' => !$this->moduleHandler->moduleExists('imagecache_external'),
    ];

    $form['media']['cache_avatar_style'] = [
      '#title' => $this->t('Cache avatar style'),
      '#description' => $this->t('This image style will be used for the avatars in the inbox.'),
      '#default_value' => $config->get('cache_avatar_style'),
      '#type' => 'select',
      '#options' => image_style_options(TRUE),
      '#disabled' => !$this->moduleHandler->moduleExists('imagecache_external'),
      '#states' => array(
        'visible' => array(
          ':input[name="cache_images"]' => array('checked' => TRUE),
        ),
      ),
    ];

    $form['media']['cache_attachment_style'] = [
      '#title' => $this->t('Cache content image style'),
      '#description' => $this->t('This image style will be used for the image attachments on content in the inbox.'),
      '#default_value' => $config->get('cache_attachment_style'),
      '#type' => 'select',
      '#options' => image_style_options(TRUE),
      '#disabled' => !$this->moduleHandler->moduleExists('imagecache_external'),
      '#states' => array(
        'visible' => array(
          ':input[name="cache_images"]' => array('checked' => TRUE),
        ),
      ),
    ];

    $form['content'] = [
      '#type' => 'details',
      '#title' => $this->t('Content'),
      '#open' => TRUE
    ];

    $filter_formats = [];
    foreach (filter_formats() as $key => $format) {
      $filter_formats[$key] = $format->label();
    }
    $form['content']['filter_format'] = [
      '#type' => 'select',
      '#title' => $this->t('Text format'),
      '#options' => $filter_formats,
      '#description' => $this->t('Select the Text format which will be used to format content and summaries.'),
      '#default_value' => $config->get('filter_format'),
    ];

    $form['outbox'] = [
      '#type' => 'details',
      '#title' => $this->t('Outbox'),
      '#open' => TRUE
    ];

    $form['outbox']['site_wide_uid'] = [
      '#title' => $this->t('Site-wide user'),
      '#type' => 'number',
      '#default_value' => $config->get('site_wide_uid'),
    ];

    $form['outbox']['process_outbox_handler'] = [
      '#title' => $this->t('Send activities'),
      '#type' => 'radios',
      '#options' => [
        '' => $this->t('Disabled'),
        'cron' => $this->t('On cron run'),
        'drush' => $this->t('With drush'),
      ],
      '#default_value' => $config->get('process_outbox_handler'),
      '#description' => $this->t('Activities in the Outbox are not send immediately, but are prepared and stored in a queue.<br />The drush commands are <strong>activitypub:prepare-outbox</strong> and <strong>activitypub:process-outbox</strong>')
    ];

    $form['inbox'] = [
      '#type' => 'details',
      '#title' => $this->t('Inbox'),
      '#open' => TRUE
    ];

    $form['inbox']['process_inbox_handler'] = [
      '#title' => $this->t('Process incoming'),
      '#type' => 'radios',
      '#options' => [
        '' => $this->t('Disabled'),
        'cron' => $this->t('On cron run'),
        'drush' => $this->t('With drush'),
      ],
      '#default_value' => $config->get('process_inbox_handler'),
      '#description' => $this->t('Activities in the Inbox that need to be processed (e.g. Create, with or without a reply) are not processed immediately, but are stored in a queue.<br />The drush command is <strong>activitypub:process-inbox</strong>')
    ];

    $form['inbox']['inbox_remove_x_days_handler'] = [
      '#title' => $this->t('Remove old activities'),
      '#type' => 'radios',
      '#options' => [
        '' => $this->t('Disabled'),
        'cron' => $this->t('On cron run'),
        'drush' => $this->t('With drush'),
      ],
      '#default_value' => $config->get('inbox_remove_x_days_handler'),
      '#description' => $this->t('Removes activities in the Inbox after a fixed period of days. Leave disabled to keep them all.<br />The drush command is <strong>activitypub:remove-old-activities</strong>'),
    ];

    $form['inbox']['inbox_remove_x_days'] = [
      '#type' => 'number',
      '#min' => 0,
      '#title' => $this->t('Older than'),
      '#field_suffix' => $this->t('days'),
      '#default_value' => $config->get('inbox_remove_x_days'),
      '#states' => [
        'invisible' => ['input[name="inbox_remove_x_days_handler"]' => ['value' => FALSE]],
        'required' => ['input[name="inbox_remove_x_days_handler"]' => ['!value' => FALSE]],
      ],
    ];

    $form['inbox']['inbox_remove_x_days_keep_unread'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Only remove read items'),
      '#description' => $this->t('Keep old activities which are not read yet. Useful in combination using the Reader module.'),
      '#default_value' => $config->get('inbox_remove_x_days_keep_unread'),
      '#states' => [
        'invisible' => ['input[name="inbox_remove_x_days_handler"]' => ['value' => FALSE]],
      ],
    ];

    $form['development'] = [
      '#type' => 'details',
      '#title' => $this->t('Development'),
      '#description' => $this->t('Various options for debugging purposes.'),
    ];
    $form['development']['log_unsaved_inbox_activity'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log unsaved activities in watchdog'),
      '#description' => $this->t('Some activities are not saved in, e.g. Update and Delete. This option allows you to store them in watchdog.'),
      '#default_value' => $config->get('log_unsaved_inbox_activity'),
    ];
    $form['development']['log_general_inbox_error'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log general inbox error requests'),
      '#default_value' => $config->get('log_general_inbox_error'),
      '#description' => $this->t('Logs the error when something completely goes wrong in the inbox request.')
    ];
    $form['development']['log_error_signature'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log error signature verification'),
      '#default_value' => $config->get('log_error_signature'),
      '#description' => $this->t('Logs the error when the signature can not be verified for incoming requests.')
    ];
    $form['development']['log_ignore_error_signature'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Ignore logging some of signature verification errors'),
      '#default_value' => $config->get('log_ignore_error_signature'),
      '#description' => $this->t('A lot of errors are due to the actor not existing anymore, which will return either 404 or 410.<br />This is because signature verification is not 100% done yet. This option allows you to ignore those types.')
    ];
    $form['development']['log_success_signature'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log success signature verification'),
      '#default_value' => $config->get('log_success_signature'),
      '#description' => $this->t('Logs a success verification for an actor.')
    ];
    $form['development']['log_followee_check'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log followee check'),
      '#description' => $this->t('When signature verification fails, the code checks whether you are following the actor to set the status and set the activity published.'),
      '#default_value' => $config->get('log_followee_check'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Validate that inbox_remove_x_days is not empty when selecting a handler.
    $inbox_remove_x_days_handler = $form_state->getValue('inbox_remove_x_days_handler');
    if (!empty($inbox_remove_x_days_handler) && empty($form_state->getValue('inbox_remove_x_days'))) {
      $form_state->setErrorByName('inbox_remove_x_days', $this->t('You can not set days to 0.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('activitypub.settings')
      ->set('avatar_user_style', $form_state->getValue('avatar_user_style'))
      ->set('avatar_user_field', $form_state->getValue('avatar_user_field'))
      ->set('header_user_field', $form_state->getValue('header_user_field'))
      ->set('header_user_style', $form_state->getValue('header_user_style'))
      ->set('avatar_default_path', $form_state->getValue('avatar_default_path'))
      ->set('attachment_content_style', $form_state->getValue('attachment_content_style'))
      ->set('cache_images', $form_state->getValue('cache_images'))
      ->set('cache_avatar_style', $form_state->getValue('cache_avatar_style'))
      ->set('cache_attachment_style', $form_state->getValue('cache_attachment_style'))
      ->set('process_outbox_handler', $form_state->getValue('process_outbox_handler'))
      ->set('process_inbox_handler', $form_state->getValue('process_inbox_handler'))
      ->set('inbox_remove_x_days_handler', $form_state->getValue('inbox_remove_x_days_handler'))
      ->set('inbox_remove_x_days', $form_state->getValue('inbox_remove_x_days'))
      ->set('inbox_remove_x_days_keep_unread', $form_state->getValue('inbox_remove_x_days_keep_unread'))
      ->set('filter_format', $form_state->getValue('filter_format'))
      ->set('log_unsaved_inbox_activity', $form_state->getValue('log_unsaved_inbox_activity'))
      ->set('log_general_inbox_error', $form_state->getValue('log_general_inbox_error'))
      ->set('log_success_signature', $form_state->getValue('log_success_signature'))
      ->set('log_followee_check', $form_state->getValue('log_followee_check'))
      ->set('log_error_signature', $form_state->getValue('log_error_signature'))
      ->set('log_ignore_error_signature', $form_state->getValue('log_ignore_error_signature'))
      ->set('site_wide_uid', $form_state->getValue('site_wide_uid'))
      ->save();
    Cache::invalidateTags(['rendered']);
    parent::submitForm($form, $form_state);
  }

}
