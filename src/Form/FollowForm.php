<?php

namespace Drupal\activitypub\Form;

use Drupal\activitypub\Entity\ActivityPubActorInterface;
use Drupal\activitypub\Services\ActivityPubUtilityInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;

class FollowForm extends FormBase {

  /**
   * The ActivityPub utility service.
   *
   * @var \Drupal\activitypub\Services\ActivityPubUtilityInterface
   */
  protected $activityPubUtility;

  /**
   * FollowForm constructor
   *
   * @param \Drupal\activitypub\Services\ActivityPubUtilityInterface $activityPubUtility
   *   The ActivityPub utility service.
   */
  public function __construct(ActivityPubUtilityInterface $activityPubUtility) {
    $this->activityPubUtility = $activityPubUtility;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('activitypub.utility')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'activitypub_follow_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ActivityPubActorInterface $actor = NULL) {

    if ($actor) {
      $form['handle'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Handle'),
        '#placeholder' => $this->t('Enter your username@domain to follow from'),
        '#description' => $this->t('You can follow me from Mastodon or any other similar application that uses ActivityPub. Search for my profile (' . $this->activityPubUtility->getActivityPubName($actor) . ') from your instance, or enter your username here and hit follow button.'),
        '#required' => TRUE,
      ];
      $form['follow'] = [
        '#type' => 'submit',
        '#value' => $this->t('Proceed to follow'),
      ];
      $form['local-actor'] = [
        '#type' => 'hidden',
        '#value' => $this->activityPubUtility->getActivityPubName($actor)
      ];
    }
    else {
      $form['info'] = ['#markup' => $this->t('No ActivityPub actor found')];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $endpoint = '';
    $handle = $form_state->getValue('handle');
    try {
      $webfinger = $this->activityPubUtility->getServer()->actor($handle)->webfinger();
      $links = $webfinger->getLinks();
      foreach ($links as $link) {
        if ($link['rel'] == 'http://ostatus.org/schema/1.0/subscribe') {
          $endpoint = $link['template'];
        }
      }
    }
    catch (\Exception $e) {
      $this->logger('activitypub')->error('Error getting subscribe endpoint: @message', ['@message' => $e->getMessage()]);
    }
    if (empty($endpoint)) {
      $form_state->setErrorByName('handle', $this->t('Could not find your subscription endpoint.'));
    }
    else {
      $form_state->setValue('endpoint', $endpoint);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $local_actor = $form_state->getValue('local-actor');
    $endpoint = $form_state->getValue('endpoint');
    $redirect = str_replace('{uri}', $local_actor, $endpoint);
    $response = new TrustedRedirectResponse($redirect);
    $form_state->setResponse($response);
  }

}
