<?php

namespace Drupal\activitypub\Form;

use Drupal\activitypub\Services\Type\TypePluginManager;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * ActivityPub type form.
 *
 * @property \Drupal\activitypub\Entity\ActivityPubTypeInterface $entity
 */
class ActivityPubTypeForm extends EntityForm {

  /**
   * The plugin manager for ActivityPub objects.
   *
   * @var \Drupal\activitypub\Services\Type\TypePluginManager
   */
  protected $typePluginManager;

  /**
   * ActivitypubTypeForm constructor
   *
   * @param \Drupal\activitypub\Services\Type\TypePluginManager $widget_plugin_manager
   */
  public function __construct(TypePluginManager $widget_plugin_manager) {
    $this->typePluginManager = $widget_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.activitypub.type')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseFormId() {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#description' => $this->t('Label for the ActivityPub type.'),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\activitypub\Entity\ActivitypubType::load',
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $this->entity->getDescription(),
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $this->entity->status(),
    ];

    $plugin_options = ['' => $this->t('- Select -')];
    foreach ($this->typePluginManager->getDefinitions() as $plugin_id => $definition) {

      /** @var \Drupal\activitypub\Services\Type\TypePluginInterface $typePlugin */
      $typePlugin = $this->typePluginManager->createInstance($plugin_id);

      if (!$typePlugin->isExposed()) {
        continue;
      }

      $plugin_options[$plugin_id] = $definition['label'];
    }

    $selected_plugin_id = '';
    if (!empty($form_state->getValue('plugin_id'))) {
      $selected_plugin_id = $form_state->getValue('plugin_id');
    }
    elseif (($plugin = $this->entity->getPlugin()) && !empty($plugin['id'])) {
      $selected_plugin_id = $this->entity->getPlugin()['id'];
    }
    $form['plugin_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Type plugin'),
      '#default_value' => $selected_plugin_id,
      '#options' => $plugin_options,
      '#ajax' => [
        'trigger_as' => ['name' => 'type_configure'],
        'callback' => '::buildAjaxTypeForm',
        'wrapper' => 'type-config-form',
        'method' => 'replace',
        'effect' => 'fade',
      ],
    ];

    $form['plugin_configuration'] = [
      '#type' => 'container',
      '#tree' => TRUE,
      '#attributes' => [
        'id' => 'type-config-form',
      ],
    ];

    $form['type_configure_button'] = [
      '#type' => 'submit',
      '#name' => 'type_configure',
      '#value' => $this->t('Configure type'),
      '#limit_validation_errors' => [['plugin_id'], ['plugin_configuration']],
      '#submit' => ['::submitAjaxTypeForm'],
      '#ajax' => [
        'callback' => '::buildAjaxTypeForm',
        'wrapper' => 'type-config-form',
      ],
      '#attributes' => ['class' => ['js-hide']],
    ];

    if (!empty($selected_plugin_id)) {

      $configuration = NULL;
      if (!empty($form_state->getValue('plugin_configuration'))) {
        $configuration = $form_state->getValue('plugin_configuration');
      }
      elseif (($plugin = $this->entity->getPlugin()) && !empty($plugin['configuration'])) {
        $configuration = $plugin['configuration'];
      }
      $this->entity->setPlugin($selected_plugin_id, $configuration);

      /** @var \Drupal\activitypub\Services\Type\TypePluginInterface $typePlugin */
      $typePlugin = $this->typePluginManager->createInstance($selected_plugin_id, $this->entity->getPlugin()['configuration']);
      $form['plugin_configuration'] += $typePlugin->buildConfigurationForm([], $form_state);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $plugin = [
      'id' => $form_state->getValue('plugin_id'),
      'configuration' => $form_state->getValue('plugin_configuration')
    ];
    $this->entity->set('plugin', $plugin);
    $result = parent::save($form, $form_state);
    $message_args = ['%label' => $this->entity->label()];
    $message = $result == SAVED_NEW
      ? $this->t('Created new ActivityPub type %label.', $message_args)
      : $this->t('Updated ActivityPub type %label.', $message_args);
    $this->messenger()->addStatus($message);
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    return $result;
  }

  /**
   * Handles form submissions for the type subform.
   *
   * @param $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function submitAjaxTypeForm($form, FormStateInterface $form_state) {
    $form_state->setRebuild();
  }

  /**
   * Handles changes ajax elements.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return mixed
   */
  public function buildAjaxTypeForm(array $form, FormStateInterface $form_state) {
    return $form['plugin_configuration'];
  }

}
