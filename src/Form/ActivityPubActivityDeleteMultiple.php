<?php

namespace Drupal\activitypub\Form;

use Drupal\activitypub\ActivityPubAccessTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Provides a activity deletion confirmation form.
 *
 * @internal
 */
class ActivityPubActivityDeleteMultiple extends ConfirmFormBase {

  use ActivityPubAccessTrait;

  /**
   * The array of activities to delete.
   *
   * @var string[][]
   */
  protected $activityInfo = [];

  /**
   * The tempstore factory.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * The node storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $storage;

  /**
   * Constructs a DeleteMultiple form object.
   *
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The tempstore factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $manager
   *   The entity type manager.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(PrivateTempStoreFactory $temp_store_factory, EntityTypeManagerInterface $manager) {
    $this->tempStoreFactory = $temp_store_factory;
    $this->storage = $manager->getStorage('activitypub_activity');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('tempstore.private'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'activitypub_activity_multiple_delete_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->formatPlural(count($this->activityInfo), 'Are you sure you want to delete this item?', 'Are you sure you want to delete these items?');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('activitypub.user.activities', ['user' => $this->currentUser()->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $this->activityInfo = $this->tempStoreFactory->get('activity_multiple_delete_confirm')->get($this->currentUser()->id());
    if (empty($this->activityInfo)) {
      return new RedirectResponse($this->getCancelUrl()->setAbsolute()->toString());
    }
    /** @var \Drupal\activitypub\Entity\ActivityPubActivityInterface[] $activities */
    $activities = $this->storage->loadMultiple(array_keys($this->activityInfo));

    $items = [];
    foreach ($activities as $activity) {
      $items[$activity->id()] = $activity->label();
    }

    $form['activities'] = [
      '#theme' => 'item_list',
      '#items' => $items,
    ];
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue('confirm') && !empty($this->activityInfo)) {

      /** @var \Drupal\activitypub\Entity\ActivityPubActivityInterface[] $activities */
      $activities = $this->storage->loadMultiple(array_keys($this->activityInfo));
      $total_count = count($activities);

      if ($activities) {
        $this->storage->delete($activities);
        $this->logger('activitypub')->notice('Deleted @count activities.', ['@count' => $total_count]);
      }

      if ($total_count) {
        $this->messenger()->addMessage($this->formatPlural($total_count, 'Deleted 1 activity.', 'Deleted @count activities.'));
      }

      $this->tempStoreFactory->get('activity_multiple_delete_confirm')->delete($this->currentUser()->id());
    }

    $form_state->setRedirect('activitypub.user.activities', ['user' => $this->currentUser()->id()]);
  }

}
