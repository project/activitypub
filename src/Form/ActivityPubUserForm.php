<?php

namespace Drupal\activitypub\Form;

use Drupal\activitypub\Services\ActivityPubFormAlterInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ActivityPubUserForm extends FormBase {

  /**
   * The ActivityPub form alter service.
   *
   * @var \Drupal\activitypub\Services\ActivityPubFormAlterInterface
   */
  protected $activityPubFormAlter;

  /**
   * UserForm constructor
   *
   * @param \Drupal\activitypub\Services\ActivityPubFormAlterInterface $activitypub_form_alter
   */
  public function __construct(ActivityPubFormAlterInterface $activitypub_form_alter) {
    $this->activityPubFormAlter = $activitypub_form_alter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('activitypub.form_alter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'activitypub_user_form';
  }

  /**
   * Check that the actor points to the user.
   *
   * @param \Drupal\user\UserInterface $user
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function currentUserCheck(UserInterface $user) {

    if ($user->id() == $this->currentUser()->id()) {
      return AccessResult::allowedIfHasPermission($user,'allow users to enable activitypub');
    }

    return AccessResult::forbidden();
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, UserInterface $user = NULL) {
    $this->activityPubFormAlter->addActivityPubSettingsFormElement($form, $form_state, $user, 'person');
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {}

}
