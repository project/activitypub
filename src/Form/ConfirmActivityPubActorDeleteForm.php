<?php

namespace Drupal\activitypub\Form;

use Drupal\activitypub\ActivityPubAccessTrait;
use Drupal\activitypub\Entity\ActivityPubActorInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\user\UserInterface;

class ConfirmActivityPubActorDeleteForm extends ConfirmFormBase {

  use ActivityPubAccessTrait;

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the Actor?');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('activitypub.user.settings', ['user' => $this->currentUser()->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'activitypub_actor_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, UserInterface $user = NULL, ActivityPubActorInterface $activitypub_actor = NULL) {
    $form = parent::buildForm($form, $form_state);

    $form['activitypub_actor'] = [
      '#type' => 'value',
      '#value' => $activitypub_actor,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\activitypub\Entity\ActivityPubActorInterface $activitypub_actor */
    $activitypub_actor = $form_state->getValue('activitypub_actor');
    $activitypub_actor->delete();
    $this->messenger()->addMessage($this->t('ActivityPub profile has been deleted.'));
    Cache::invalidateTags(['user:' . $activitypub_actor->getOwnerId()]);
    $form_state->setRedirectUrl(Url::fromRoute('activitypub.user.settings', ['user' => $activitypub_actor->getOwnerId()]));
  }

}
