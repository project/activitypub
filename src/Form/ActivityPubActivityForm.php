<?php

namespace Drupal\activitypub\Form;

use Drupal\activitypub\Entity\ActivityPubActivityInterface;
use Drupal\activitypub\Services\ActivityPubUtilityInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * ActivityPub Activity form.
 *
 * @property \Drupal\activitypub\Entity\ActivityPubActivityInterface $entity
 */
class ActivityPubActivityForm extends ContentEntityForm {

  /**
   * The ActivityPub utility service.
   *
   * @var \Drupal\activitypub\Services\ActivityPubUtilityInterface
   */
  protected $activityPubUtitility;

  /**
   * Constructs a ActivityPubActivityForm object.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\activitypub\Services\ActivityPubUtilityInterface $activitypub_utility
   *   The utility service.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, EntityTypeBundleInfoInterface $entity_type_bundle_info, TimeInterface $time, ActivityPubUtilityInterface $activitypub_utility) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->activityPubUtitility = $activitypub_utility;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('activitypub.utility')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['type'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Type'),
      '#default_value' => $this->entity->getType(),
      '#description' => $this->t('Add an activity type, e.g. Create, Like, Announce, Follow, Accept, Delete, Undo ..')
    ];

    $form['collection'] = [
      '#type' => 'select',
      '#title' => $this->t('Collection'),
      '#default_value' => $this->entity->getCollection(),
      '#options' => [
        ActivityPubActivityInterface::INBOX => $this->t('Inbox'),
        ActivityPubActivityInterface::OUTBOX => $this->t('Outbox'),
      ],
    ];

    $form['actor'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Actor'),
      '#default_value' => $this->entity->getActor(),
    ];

    $form['object'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Object'),
      '#default_value' => $this->entity->getObject(),
    ];

    $form['reply'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Reply'),
      '#default_value' => $this->entity->getReply(),
    ];

    $form['external_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('External ID'),
      '#default_value' => $this->entity->getExternalId(),
    ];

    $form['visibility'] = [
      '#type' => 'select',
      '#title' => $this->t('Post privacy'),
      '#options' => $this->activityPubUtitility->getVisibilityOptions(),
      '#default_value' => $this->entity->getVisibility(),
    ];

    $config_options = ['' => $this->t('- None -')];
    foreach ($this->entityTypeManager->getStorage('activitypub_type')->loadMultiple() as $type) {
      $config_options[$type->id()] = $type->label();
    }
    $form['config_id'] = [
      '#type' => 'select',
      '#title' => $this->t('ActivityPub Type'),
      '#default_value' => $this->entity->getConfigID(),
      '#options' => $config_options,
    ];

    $entity_type_type = 'select';
    $entity_type_description = '';
    $entity_type_options = ['' => $this->t('- None -')];
    $entity_type_id = $this->entity->getTargetEntityTypeId();
    foreach ($this->entityTypeManager->getDefinitions() as $definition) {
      if ($definition instanceof ContentEntityTypeInterface) {
        $entity_type_options[$definition->id()] = $definition->getLabel();
      }
    }
    if (!empty($entity_type_id) && !isset($entity_type_options[$entity_type_id])) {
      $entity_type_type = 'textfield';
      $entity_type_description = $this->t('Warning: unknown entity type');
    }
    $form['entity_type_id'] = [
      '#type' => $entity_type_type,
      '#title' => $this->t('Entity Type ID'),
      '#default_value' => $entity_type_id,
      '#options' => $entity_type_options,
      '#description' => $entity_type_description,
    ];

    $form['entity_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Entity ID'),
      '#default_value' => $this->entity->getTargetEntityId(),
    ];

    $form['processed'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Processed'),
      '#default_value' => $this->entity->isProcessed(),
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Status'),
      '#default_value' => $this->entity->isPublished(),
    ];

    $form['queued'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Queued'),
      '#default_value' => $this->entity->isQueued(),
    ];

    $form['is_read'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Read'),
      '#default_value' => $this->entity->isRead(),
    ];

    $form['mute'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Mute'),
      '#default_value' => $this->entity->isMuted(),
    ];

    if ($this->entity->isNew()) {
      $form['#title'] = $this->t('Add activity');

      /** @var \Drupal\activitypub\Entity\Storage\ActivityPubActorStorageInterface $storage */
      $storage = $this->entityTypeManager->getStorage('activitypub_actor');
      $actor = $storage->loadActorByEntityIdAndType($this->currentUser()->id(), 'person');
      $form['actor']['#default_value'] = Url::fromRoute('activitypub.user.self', ['user' => $this->currentUser()->id(), 'activitypub_actor' => $actor->getName()], ['absolute' => TRUE])->toString();
    }
    else {
      $form['#title'] = $this->t('Edit activity @id', ['@id' => $this->entity->id()]);
    }

    $form['to'] = [
      '#type' => 'textarea',
      '#title' => $this->t('To'),
      '#default_value' => $this->entity->getToRaw(),
      '#description' => $this->t('Add URL\'s of (remote) users line per line. Only add those who do not follow you.'),
    ];

    $form['payload'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Payload'),
      '#default_value' => $this->entity->getPayLoad(),
      '#description' => $this->t('Data which was sent with the request'),
      '#access' => $this->currentUser()->hasPermission('allow users to view the payload'),
    ];

    $form['context'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Context'),
      '#default_value' => $this->entity->getContext(),
      '#description' => $this->t('Context data'),
      '#access' => $this->currentUser()->hasPermission('allow users to view the context'),
    ];

    $this->alterForm($form);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);

    if (in_array($this->getRouteMatch()->getRouteName(), ['entity.activitypub_activity.add_form_follow', 'reader.sources'])) {
      $message_args = ['%label' => $this->entity->getObject()];
      $message = $result == SAVED_NEW
        ? $this->t('Started following %label.', $message_args)
        : $this->t('Updated %label.', $message_args);
    }
    else {
      $message_args = ['%label' => $this->entity->label()];
      $message = $result == SAVED_NEW
        ? $this->t('Created new ActivityPub activity %label.', $message_args)
        : $this->t('Updated ActivityPub activity %label.', $message_args);
    }
    $this->messenger()->addStatus($message);
    Cache::invalidateTags(['user:' . $this->entity->getOwnerId()]);
    $form_state->setRedirectUrl(Url::fromRoute('activitypub.user.activities', ['user' => $this->entity->getOwnerId()]));
    return $result;
  }

  /**
   * Returns the action form element for the current entity form.
   */
  protected function actionsElement(array $form, FormStateInterface $form_state) {
    $element = parent::actionsElement($form, $form_state);

    if (in_array($this->getRouteMatch()->getRouteName(), ['entity.activitypub_activity.add_form_follow', 'reader.sources'])) {
      $element['submit']['#value'] = $this->t('Follow');
    }

    return $element;
  }

  /**
   * Alters the form depending on the route.
   *
   * @param $form
   */
  protected function alterForm(&$form) {
    if (in_array($this->getRouteMatch()->getRouteName(), ['entity.activitypub_activity.add_form_follow', 'reader.sources'])) {
      $form['type']['#default_value'] = 'Follow';
      $form['collection']['#default_value'] = ActivityPubActivityInterface::OUTBOX;
      $form['config_id']['#default_value'] = 'follow';
      $form['status']['#default_value'] = FALSE;
      $form['object']['#title'] = $this->t('User URL');
      $form['#title'] = $this->t('Follow user');

      if (!empty($_GET['follow'])) {
        $form['object']['#default_value'] = $_GET['follow'];
      }

      foreach (Element::children($form) as $child) {
        if (!in_array($child, ['object'])) {
          $form[$child]['#access'] = FALSE;
        }
      }
    }
  }

}
