<?php

namespace Drupal\activitypub\Event;

use Drupal\activitypub\Entity\ActivityPubActivityInterface;
use Drupal\Component\EventDispatcher\Event;

class ActivityAudienceEvent extends Event {

  const ALTER_AUDIENCE = 'activitypub_alter_audience_event';

  /**
   * The activity for which the audience is built.
   *
   * @var \Drupal\activitypub\Entity\ActivityPubActivityInterface
   */
  protected $activity;

  /**
   * The audience array.
   *
   * @var array
   *   A collection with following keys:
   *     - to
   *     - cc
   *     - mention
   */
  protected $audience;

  /**
   * Constructor.
   *
   * @param \Drupal\activitypub\Entity\ActivityPubActivityInterface $activity
   *   The activity.
   */
  public function __construct(ActivityPubActivityInterface $activity, array $audience) {
    $this->activity = $activity;
    $this->audience = $audience;
  }

  /**
   * Getter for the audience array.
   *
   * @return array
   *   The visibility options array.
   */
  public function getAudience() {
    return $this->audience;
  }

  /**
   * Setter for the audience array.
   *
   * @param $audience
   */
  public function setAudience($audience) {
    $this->audience = $audience;
  }

}