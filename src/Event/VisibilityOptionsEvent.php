<?php

namespace Drupal\activitypub\Event;

use Drupal\Component\EventDispatcher\Event;

class VisibilityOptionsEvent extends Event {

  const ADD_OPTIONS = 'activitypub_visibility_options_event';

  /**
   * The array with visibility options.
   *
   * @var array
   */
  protected $visibilityOptions;

  /**
   * Constructor.
   *
   * @param array $visibility_options
   *   The visibility options.
   */
  public function __construct(array $visibility_options) {
    $this->visibilityOptions = $visibility_options;
  }

  /**
   * Getter for the visibility options array.
   *
   * @return array
   *   The visibility options array.
   */
  public function getVisibilityOptions() {
    return $this->visibilityOptions;
  }

  /**
   * Add a visibility option.
   *
   * @param int $id
   * @param string $label
   */
  public function addVisibilityOption(int $id, string $label) {
    if (!isset($this->visibilityOptions[$id])) {
      $this->visibilityOptions[$id] = $label;
    }
  }

}