<?php

namespace Drupal\activitypub\Controller;

use Drupal\activitypub\Entity\ActivityPubActivityInterface;
use Drupal\comment\CommentInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\node\NodeInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class EntityController extends BaseController {

  /**
   * Node self routing callback with activity+json accept header.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @param \Drupal\node\NodeInterface $node
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function node(Request $request, NodeInterface $node) {
    return $this->json($request, $node);
  }

  /**
   * Comment self routing callback with activity+json accept header.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @param \Drupal\comment\CommentInterface $comment
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function comment(Request $request, CommentInterface $comment) {
    return $this->json($request, $comment);
  }

  /**
   * Entity self routing callback with activity+json accept header.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *
   * @return array|\Symfony\Component\HttpFoundation\JsonResponse
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function json(Request $request, EntityInterface $entity) {
    $this->killSwitch->trigger();

    /** @var \Drupal\activitypub\Entity\Storage\ActivityPubActorStorageInterface $actorStorage */
    $actorStorage = $this->entityTypeManager()->getStorage('activitypub_actor');
    $actor = $actorStorage->loadActorByEntityIdAndType($entity->getOwnerId(), 'person');

    /** @var \Drupal\activitypub\Entity\Storage\ActivityPubActivityStorageInterface $activityStorage */
    /** @var \Drupal\activitypub\Entity\ActivityPubActivityInterface $activity */
    $activity = NULL;
    $activityStorage = $this->entityTypeManager()->getStorage('activitypub_activity');
    $activities = $activityStorage->loadByProperties(['entity_type_id' => $entity->getEntityTypeId(), 'entity_id' => $entity->id(), 'collection' => 'outbox']);
    if (!empty($activities)) {
      $activity = array_shift($activities);
    }

    if ($actor && $activity) {
      $build = $activity->buildActivity();
      if (is_array($build['object'])) {
        $output = $build['object'];
      }
      else {
        $output = $build;
      }
      $output['@context'] = ActivityPubActivityInterface::CONTEXT_URL;
      $response = new JsonResponse($output, 200);
      $response->headers->set('Content-Type', 'application/activity+json');
      return $response;
    }
    else {
      $accept = $request->getAcceptableContentTypes();
      if (in_array('text/html', $accept)) {
        $request->setRequestFormat('html');

        $view_mode = 'full';
        if ($entity->getEntityTypeId() == 'comment') {
          $view_mode = 'indieweb_microformat';
          if (empty($entity->getOwnerId())) {
            $view_mode = 'full';
          }
        }

        return $this->entityTypeManager()->getViewBuilder($entity->getEntityTypeId())->view($entity, $view_mode);
      }
      return new JsonResponse(NULL, 404);
    }
  }

}
