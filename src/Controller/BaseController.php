<?php

namespace Drupal\activitypub\Controller;

use Drupal\activitypub\ActivityPubAccessTrait;
use Drupal\activitypub\Services\ActivityPubSignatureInterface;
use Drupal\activitypub\Services\ActivityPubUtilityInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\PageCache\ResponsePolicy\KillSwitch;
use Drupal\Core\Pager\PagerManagerInterface;
use Drupal\Core\Path\PathMatcherInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class BaseController extends ControllerBase {

  use ActivityPubAccessTrait;

  /**
   * The ActivityPub Utility service.
   *
   * @var \Drupal\activitypub\Services\ActivityPubUtilityInterface
   */
  protected $activityPubUtility;

  /**
   * The ActivityPub signature service.
   *
   * @var \Drupal\activitypub\Services\ActivityPubSignatureInterface
   */
  protected $activityPubSignature;

  /**
   * The path matcher service.
   *
   * @var \Drupal\Core\Path\PathMatcherInterface
   */
  protected $pathMatcher;

  /**
   * The pager manager.
   *
   * @var \Drupal\Core\Pager\PagerManagerInterface
   */
  protected $pagerManager;

  /**
   * Kill switch.
   *
   * @var \Drupal\Core\PageCache\ResponsePolicy\KillSwitch
   */
  protected $killSwitch;

  /**
   * BaseController constructor
   *
   * @param \Drupal\Core\Path\PathMatcherInterface $path_matcher
   *   The path matcher
   * @param \Drupal\activitypub\Services\ActivityPubUtilityInterface $activitypub_utility
   *   The ActivityPub utility service
   * @param \Drupal\activitypub\Services\ActivityPubSignatureInterface $activitypub_signature
   *   The ActivityPub Signature service.
   * @param \Drupal\Core\Pager\PagerManagerInterface $pager_manager
   *   The pager manager
   */
  public function __construct(PathMatcherInterface $path_matcher, ActivityPubUtilityInterface $activitypub_utility, ActivityPubSignatureInterface $activitypub_signature, PagerManagerInterface $pager_manager, KillSwitch $kill_switch) {
    $this->activityPubUtility = $activitypub_utility;
    $this->activityPubSignature = $activitypub_signature;
    $this->pathMatcher = $path_matcher;
    $this->pagerManager = $pager_manager;
    $this->killSwitch = $kill_switch;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('path.matcher'),
      $container->get('activitypub.utility'),
      $container->get('activitypub.signature'),
      $container->get('pager.manager'),
      $container->get('page_cache_kill_switch')
    );
  }

}
