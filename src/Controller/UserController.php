<?php

namespace Drupal\activitypub\Controller;

use Drupal\activitypub\Entity\ActivityPubActivityInterface;
use Drupal\activitypub\Entity\ActivityPubActorInterface;
use Drupal\activitypub\Services\ActivityPubSignatureInterface;
use Drupal\activitypub\Services\ActivityPubUtilityInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Language\Language;
use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Url;
use Drupal\user\UserInterface;
use Drupal\views\Views;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class UserController extends BaseController {

  /**
   * The ActivityPub utility service.
   *
   * @var \Drupal\activitypub\Services\ActivityPubUtilityInterface
   */
  protected $activityPubUtility;

  /**
   * The ActivityPub signature service.
   *
   * @var \Drupal\activitypub\Services\ActivityPubSignatureInterface
   */
  protected $activityPubSignature;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * SelfController constructor
   *
   * @param \Drupal\activitypub\Services\ActivityPubUtilityInterface $activitypub_utility
   *   The ActivityPub utility service.
   * @param \Drupal\activitypub\Services\ActivityPubSignatureInterface $activitypub_signature
   *   The ActivityPub Signature service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   */
  public function __construct(ActivityPubUtilityInterface $activitypub_utility, ActivityPubSignatureInterface $activitypub_signature, LanguageManagerInterface $language_manager) {
    $this->activityPubUtility = $activitypub_utility;
    $this->activityPubSignature = $activitypub_signature;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('activitypub.utility'),
      $container->get('activitypub.signature'),
      $container->get('language_manager')
    );
  }

  /**
   * Self routing callback.
   *
   * @param \Drupal\user\UserInterface $user
   * @param \Drupal\activitypub\Entity\ActivityPubActorInterface $activitypub_actor
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function self(UserInterface $user, ActivityPubActorInterface $activitypub_actor) {
    $undefined_lang = $this->languageManager->getLanguage(Language::LANGCODE_NOT_SPECIFIED);
    $metadata = new CacheableMetadata();
    $canonical = Url::fromRoute('entity.user.canonical', ['user' => $user->id()], ['absolute' => TRUE, 'language' => $undefined_lang])->toString(TRUE);
    $inbox = Url::fromRoute('activitypub.inbox', ['user' => $user->id(), 'activitypub_actor' => $activitypub_actor->getName()], ['absolute' => TRUE])->toString(TRUE);
    $outbox = Url::fromRoute('activitypub.outbox', ['user' => $user->id(), 'activitypub_actor' => $activitypub_actor->getName()], ['absolute' => TRUE])->toString(TRUE);
    $following = Url::fromRoute('activitypub.following', ['user' => $user->id(), 'activitypub_actor' => $activitypub_actor->getName()], ['absolute' => TRUE])->toString(TRUE);
    $followers = Url::fromRoute('activitypub.followers', ['user' => $user->id(), 'activitypub_actor' => $activitypub_actor->getName()], ['absolute' => TRUE])->toString(TRUE);
    $metadata->addCacheableDependency($canonical);
    $metadata->addCacheableDependency($inbox);
    $metadata->addCacheableDependency($outbox);
    $metadata->addCacheableDependency($following);
    $metadata->addCacheableDependency($followers);
    $metadata->addCacheTags(['user:' . $user->id()]);

    $data = [
      'type' => 'Person',
      '@context' => [ActivityPubActivityInterface::CONTEXT_URL, ActivityPubActivityInterface::SECURITY_URL],
      'name' => $activitypub_actor->getName(),
      'preferredUsername' => $activitypub_actor->getName(),
      'id' => $this->activityPubUtility->getActivityPubID($activitypub_actor),
      'url' => $canonical->getGeneratedUrl(),
      'inbox' => $inbox->getGeneratedUrl(),
      'outbox' => $outbox->getGeneratedUrl(),
      'following' => $following->getGeneratedUrl(),
      'followers' => $followers->getGeneratedUrl(),
      'publicKey' => [
        'owner' => $this->activityPubUtility->getActivityPubID($activitypub_actor),
        'id' => $this->activityPubUtility->getActivityPubID($activitypub_actor) . '#main-key',
        'publicKeyPem' => $this->activityPubSignature->getPublicKey($activitypub_actor->getName()),
      ],
    ];

    if ($image_url = $this->activityPubUtility->getActivityPubUserImage($user)) {
      $image = [
        'type' => 'Image',
        'name' => $activitypub_actor->getName(),
        'url' => $image_url,
      ];
      $data['icon'] = (object) $image;
    }

    if ($image_url = $this->activityPubUtility->getActivityPubUserImage($user, 'header')) {
      $image = [
        'type' => 'Image',
        'name' => $activitypub_actor->getName(),
        'url' => $image_url,
      ];
      $data['image'] = (object) $image;
    }

    if ($summary = $activitypub_actor->getSummary()) {
      $data['summary'] = trim(check_markup($summary, $this->config('activitypub.settings')->get('filter_format')));
    }

    $response = new CacheableJsonResponse($data);
    $response->addCacheableDependency($metadata);
    $response->headers->set('Content-Type', 'application/activity+json');
    return $response;
  }

  /**
   * User self routing callback with activity+json accept header.
   *
   * @param \Drupal\user\UserInterface $user
   *
   * @return \Drupal\Core\Cache\CacheableJsonResponse|\Symfony\Component\HttpFoundation\JsonResponse
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function selfJson(UserInterface $user) {
    /** @var \Drupal\activitypub\Entity\Storage\ActivityPubActorStorageInterface $storage */
    $storage = $this->entityTypeManager()->getStorage('activitypub_actor');
    $actor = $storage->loadActorByEntityIdAndType($user->id(), 'person');
    if ($actor) {
      return $this->self($user, $actor);
    }
    else {
      return new JsonResponse(NULL, 404);
    }
  }

  /**
   * Activities routing callback.
   *
   * @param \Drupal\user\UserInterface $user
   *
   * @return array
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function activities(UserInterface $user) {
    /** @var \Drupal\activitypub\Entity\Storage\ActivityPubActorStorageInterface $storage */
    $storage = $this->entityTypeManager()->getStorage('activitypub_actor');
    $actor = $storage->loadActorByEntityIdAndType($user->id(), 'person');
    if (!$actor) {

      $settings_link = Url::fromRoute('activitypub.user.settings', ['user' => $user->id()])->toString();

      return [
        'info' => ['#markup' => '<p>' . $this->t('ActivityPub is not enabled for your account.') . '</p>'],
        'link' => ['#markup' => '<p>' . $this->t('<a href="@url">Enable ActivityPub</a>', ['@url' => $settings_link]) . '</p>'],
      ];
    }
    else {

      $build = [];

      $conditions = [
        'uid' => $this->currentUser()->id(),
        'queued' => 1,
      ];
      $total = $this->entityTypeManager()->getStorage('activitypub_activity')->getActivityCount($conditions);
      $build['queue'] = [
        '#markup' => '<p>' . $this->t('Items in queue: @count', ['@count' => $total]) . '</p>',
        '#weight' => -10,
      ];

      /** @var \Drupal\views\ViewEntityInterface $view */
      $view = NULL;
      try {
        $view = $this->entityTypeManager()->getStorage('view')->load('activitypub_user_activities');
      }
      catch (\Exception $ignored) {}
      if ($view && $view->status()) {
        $view = Views::getView('activitypub_user_activities');
        $build['list'] = $view->render('block_1');
      }
      else {
        $build['list'] = $this->entityTypeManager()->getListBuilder('activitypub_activity')->render();
      }

      return $build;
    }
  }
}
