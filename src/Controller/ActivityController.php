<?php

namespace Drupal\activitypub\Controller;

use Drupal\activitypub\Entity\ActivityPubActivityInterface;
use Drupal\activitypub\Services\ActivityPubProcessClientInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

class ActivityController extends ControllerBase {

  /**
   * The ActivityPub process client service.
   *
   * @var \Drupal\activitypub\Services\ActivityPubProcessClientInterface
   */
  protected $activityPubProcessClient;

  /**
   * ActivityController constructor
   *
   * @param \Drupal\activitypub\Services\ActivityPubProcessClientInterface $activitypub_process_client
   */
  public function __construct(ActivityPubProcessClientInterface $activitypub_process_client) {
    $this->activityPubProcessClient = $activitypub_process_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('activitypub.process.client')
    );
  }

  /**
   * View activity routing callback.
   *
   * @param \Drupal\activitypub\Entity\ActivityPubActivityInterface $activitypub_activity
   *
   * @return string[]
   */
  public function view(ActivityPubActivityInterface $activitypub_activity) {
    return [
      '#markup' => 'Activity ' . $activitypub_activity->id(),
    ];
  }

  /**
   * Queue activity routing callback.
   *
   * @param \Drupal\activitypub\Entity\ActivityPubActivityInterface $activitypub_activity
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function queue(ActivityPubActivityInterface $activitypub_activity) {
    if ($activitypub_activity->canBeQueued()) {
      $activitypub_activity
        ->set('processed', FALSE)
        ->set('queued', TRUE)
        ->save();

      // TODO move to event dispatcher when refactoring the plugins.
      $queue = NULL;
      $extra_data = [];
      if ($activitypub_activity->getCollection() == ActivityPubActivityInterface::INBOX && ($activitypub_activity->getReply() || in_array($activitypub_activity->getType(), ['Like', 'Announce']))) {
        $activityPubType = $this->entityTypeManager()->getStorage('activitypub_type')->load('context');
        if ($activityPubType && $activityPubType->isEnabled()) {
          $queue = ACTIVITYPUB_INBOX_QUEUE;
          $extra_data = ['config_id' => 'context'];
        }
      }

      $this->activityPubProcessClient->createQueueItem($activitypub_activity, $queue, $extra_data);
      $this->messenger()->addMessage($this->t('Activity @id has been added to the queue.', ['@id' => $activitypub_activity->id()]));
    }
    else {
      $this->messenger()->addWarning($this->t('The activity can not be added to the queue.'));
    }
    return new RedirectResponse(Url::fromRoute('activitypub.user.activities', ['user' => $activitypub_activity->getOwnerId()])->toString());
  }

  /**
   * Undo activity routing callback.
   *
   * @param \Drupal\activitypub\Entity\ActivityPubActivityInterface $activitypub_activity
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function undo(ActivityPubActivityInterface $activitypub_activity) {
    if ($activitypub_activity->canBeUndone()) {

      $values = [
        'type' => 'Undo',
        'collection' => ActivityPubActivityInterface::OUTBOX,
        'config_id' => 'undo',
        'actor' => $activitypub_activity->getActor(),
        'object' => $activitypub_activity->getObject(),
        'external_id' => $activitypub_activity->toUrl('canonical', ['absolute' => TRUE])->toString(),
      ];
      /** @var \Drupal\activitypub\Entity\ActivityPubActivityInterface $activity */
      $activity = $this->entityTypeManager()->getStorage('activitypub_activity')->create($values);
      $activity->save();
      $this->activityPubProcessClient->createQueueItem($activity);

      $this->messenger()->addMessage($this->t('An "Undo" activity has been created for activity @id and added to the queue.', ['@id' => $activitypub_activity->id()]));
    }
    else {
      $this->messenger()->addWarning($this->t('The activity can not be undone.'));
    }
    return new RedirectResponse(Url::fromRoute('activitypub.user.activities', ['user' => $activitypub_activity->getOwnerId()])->toString());
  }
}
