<?php

namespace Drupal\activitypub\Controller;

use Drupal\activitypub\Entity\ActivityPubTypeInterface;
use Drupal\Core\Controller\ControllerBase;

class ActivityTypeController extends ControllerBase {

  /**
   * Enable or disables the status.
   *
   * @param \Drupal\activitypub\Entity\ActivityPubTypeInterface $activitypub_type
   *   The ActivityPub Type.
   * @param string $op
   *   The operation to perform 'enable' or 'disable'.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect back to the search settings page.
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function setStatus(ActivityPubTypeInterface $activitypub_type, $op) {
    $activitypub_type->$op()->save();

    if ($op == 'enable') {
      $this->messenger()->addStatus($this->t('The %label ActivityPub Type has been enabled.', ['%label' => $activitypub_type->label()]));
    }
    elseif ($op == 'disable') {
      $this->messenger()->addStatus($this->t('The %label ActivityPub Type has been disabled.', ['%label' => $activitypub_type->label()]));
    }

    $url = $activitypub_type->toUrl('collection');
    return $this->redirect($url->getRouteName(), $url->getRouteParameters(), $url->getOptions());
  }

}
