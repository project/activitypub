<?php

namespace Drupal\activitypub\Plugin\Search;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessibleInterface;
use Drupal\search\Plugin\SearchPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\activitypub\Services\ResolveServiceInterface;

/**
 * Executes a search for users or content on the fediverse.
 *
 * @SearchPlugin(
 *   id = "activitypub_search",
 *   title = @Translation("Activitypub")
 * )
 */
class ActivitypubSearch extends SearchPluginBase implements AccessibleInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The activitypub actors and activities remote services.
   *
   * @var \Drupal\activitypub\Services\ResolveServiceInterface
   */
  protected $activityPubResolveService;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('current_user'),
      $container->get('activitypub.resolve_service'),
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * Creates a ActivitypubSearch object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\activitypub\Services\ResolveServiceInterface $resolve_service
   *   The Activitypub remote resolve service.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, AccountInterface $current_user, ResolveServiceInterface $resolve_service, array $configuration, $plugin_id, $plugin_definition) {
    $this->entityTypeManager = $entity_type_manager;
    $this->currentUser = $current_user;
    $this->activityPubResolveService = $resolve_service;
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->addCacheTags(['activitypub_remote_lookup']);
  }

  /**
   * {@inheritdoc}
   */
  public function access($operation = 'view', AccountInterface $account = NULL, $return_as_object = FALSE) {
    $result = AccessResult::allowedIf(!empty($account) && $account->hasPermission('resolve remote activitypub activities and actors'))->cachePerPermissions();
    return $return_as_object ? $result : $result->isAllowed();
  }

  /**
   * {@inheritdoc}
   */
  public function execute() {
    $results = [];
    if (!$this->isSearchExecutable()) {
      return $results;
    }

    // Process the keywords.
    $keys = $this->keywords;

    $lookup_results = $this->activityPubResolveService->resolveQuery($keys);
    // @todo add local things.
    foreach ($lookup_results['accounts'] as $account) {
      $result = [
        'title' => $account['preferredUsername'],
        'link' => $account['url'],
        // todo use interaction link: Url::fromRoute('activitypub.authorize_interaction', [], ['query' => ['uri' => $account['url']]])->toString(),
        'snippet' => $account['summary'] . "\n" . $this->t("Person. Remote URL:") . $account['url'],
      ];
      $this->addCacheableDependency($account);
      $results[] = $result;
    }

    foreach ($lookup_results['statuses'] as $activity) {
      $result = [
        'title' => $activity['id'],
        'link' => $activity['url'],
        // todo use interaction link: Url::fromRoute('activitypub.authorize_interaction', [], ['query' => ['uri' => $activity['url']]])->toString(),
        'snippet' => strip_tags($activity['content']) . "\n" . t("Activity. Remote URL:") . $activity['url'],
      ];
      $this->addCacheableDependency($activity);
      $results[] = $result;
    }

    return $results;
  }

  /**
   * {@inheritdoc}
   */
  public function getHelp() {
    return [
      'list' => [
        '#theme' => 'item_list',
        '#items' => [
          $this->t('ActivityPub search looks for remote users as @name@example.com, name@example.com or https://example.com/path_to/user.'),
          $this->t('It also looks for remote activities giving the identifier url of it.'),
        ],
      ],
    ];
  }

}
