<?php

namespace Drupal\activitypub\Plugin\views\filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\filter\ManyToOne;

/**
 * Extends config_id filter to use dropdowns.
 *
 * @ViewsFilter("activitypub_config_id_select")
 */
class ActivityConfigIDSelect extends ManyToOne {

  protected $valueFormType = 'select';

  /**
   * {@inheritdoc}
   */
  protected function valueForm(&$form, FormStateInterface $form_state) {
    parent::valueForm($form, $form_state);
    $form['value']['#options'] = $this->getEntityTypes();
  }

  /**
   * {@inheritdoc}
   */
  public function adminSummary() {
    if (!empty($this->value)) {
      return implode(', ', array_keys($this->value));
    }
    else {
      return 'all';
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validate() {
    // Do not validate.
    return [];
  }

  /**
   * Get list of entity types formatted for options list.
   */
  protected function getEntityTypes() {
    $options = [];
    foreach (\Drupal::entityTypeManager()->getStorage('activitypub_type')->loadMultiple() as $type) {
      $options[$type->id()] = $type->label();
    }
    asort($options);
    return $options;
  }

}
