<?php

namespace Drupal\activitypub\Plugin\views\filter;

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\filter\ManyToOne;

/**
 * Extends entity_type_id filter to use dropdowns.
 *
 * @ViewsFilter("activitypub_entity_type_id_select")
 */
class ActivityEntityTypeIDSelect extends ManyToOne {

  protected $valueFormType = 'select';

  /**
   * {@inheritdoc}
   */
  protected function valueForm(&$form, FormStateInterface $form_state) {
    parent::valueForm($form, $form_state);
    $form['value']['#options'] = $this->getEntityTypes();
  }

  /**
   * {@inheritdoc}
   */
  public function adminSummary() {
    if (!empty($this->value)) {
      return implode(', ', array_keys($this->value));
    }
    else {
      return 'all';
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validate() {
    // Do not validate.
    return [];
  }

  /**
   * Get list of entity types formatted for options list.
   */
  protected function getEntityTypes() {
    $options = [];
    foreach (\Drupal::entityTypeManager()->getDefinitions() as $entity_type_id => $definition) {
      if ($definition instanceof ContentEntityTypeInterface) {
        $options[$entity_type_id] = $definition->getLabel();
      }
    }
    asort($options);
    return $options;
  }

}
