<?php

namespace Drupal\activitypub\Plugin\views\filter;

use Drupal\activitypub\Entity\ActivityPubActivityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\filter\ManyToOne;

/**
 * Extends type filter to use dropdowns.
 *
 * @ViewsFilter("activitypub_type_select")
 */
class ActivityTypeSelect extends ManyToOne {

  protected $valueFormType = 'select';

  /**
   * {@inheritdoc}
   */
  protected function valueForm(&$form, FormStateInterface $form_state) {
    parent::valueForm($form, $form_state);
    $options = \Drupal::entityTypeManager()->getStorage('activitypub_activity')->getFieldOptions('type');

    // Sensible defaults in case nothing is there.
    $options += [
      'Accept' => 'Accept',
      'Announce' => 'Announce',
      'Create' => 'Create',
      'Delete' => 'Delete',
      'emojiReaction' => 'emojiReaction',
      'Follow' => 'Follow',
      'Invite' => 'Invite',
      'Like' => 'Like',
      'Reject' => 'Reject',
      'Undo' => 'Undo',
      'Update' => 'Update',
    ];
    $form['value']['#options'] = $options;
  }

  /**
   * {@inheritdoc}
   */
  public function adminSummary() {
    if (!empty($this->value)) {
      return implode(', ', array_keys($this->value));
    }
    else {
      return 'all';
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validate() {
    // Do not validate.
    return [];
  }

}
