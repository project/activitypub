<?php

namespace Drupal\activitypub\Plugin\views\filter;

use Drupal\activitypub\Entity\ActivityPubActivityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\filter\ManyToOne;

/**
 * Extends visibility filter to use dropdowns.
 *
 * @ViewsFilter("activitypub_visibility_select")
 */
class ActivityVisibilitySelect extends ManyToOne {

  protected $valueFormType = 'select';

  /**
   * {@inheritdoc}
   */
  protected function valueForm(&$form, FormStateInterface $form_state) {
    parent::valueForm($form, $form_state);

    $options = \Drupal::service('activitypub.utility')->getVisibilityOptions();
    $form['value']['#options'] = $options;
  }

  /**
   * {@inheritdoc}
   */
  public function adminSummary() {
    if (!empty($this->value)) {
      return implode(', ', array_keys($this->value));
    }
    else {
      return 'all';
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validate() {
    // Do not validate.
    return [];
  }

}
