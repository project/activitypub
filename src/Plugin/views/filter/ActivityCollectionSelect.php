<?php

namespace Drupal\activitypub\Plugin\views\filter;

use Drupal\activitypub\Entity\ActivityPubActivityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\filter\ManyToOne;

/**
 * Extends collection filter to use dropdowns.
 *
 * @ViewsFilter("activitypub_collection_select")
 */
class ActivityCollectionSelect extends ManyToOne {

  protected $valueFormType = 'select';

  /**
   * {@inheritdoc}
   */
  protected function valueForm(&$form, FormStateInterface $form_state) {
    parent::valueForm($form, $form_state);

    $options = [
      ActivityPubActivityInterface::INBOX => $this->t('Inbox'),
      ActivityPubActivityInterface::OUTBOX => $this->t('Outbox'),
    ];
    $form['value']['#options'] = $options;
  }

  /**
   * {@inheritdoc}
   */
  public function adminSummary() {
    if (!empty($this->value)) {
      return implode(', ', array_keys($this->value));
    }
    else {
      return 'all';
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validate() {
    // Do not validate.
    return [];
  }

}
