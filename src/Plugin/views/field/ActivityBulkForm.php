<?php

namespace Drupal\activitypub\Plugin\views\field;

use Drupal\views\Plugin\views\field\BulkForm;

/**
 * Defines an activity operations bulk form element.
 *
 * @ViewsField("activity_bulk_form")
 */
class ActivityBulkForm extends BulkForm {

  /**
   * {@inheritdoc}
   */
  protected function emptySelectedMessage() {
    return $this->t('No activities selected.');
  }

}
