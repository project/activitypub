<?php

namespace Drupal\activitypub\EventSubscriber;

use Drupal\activitypub\Entity\ActivityPubActivityInterface;
use Drupal\activitypub\Services\ActivityPubProcessClientInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\scheduler\Event\SchedulerNodeEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Scheduler Node subscriber.
 */
class SchedulerNodeSubscriber implements EventSubscriberInterface {

  /**
   * The ActivityPub Activity storage.
   *
   * @var \Drupal\activitypub\Entity\Storage\ActivityPubActivityStorageInterface
   */
  protected $activityStorage;

  /**
   * The process client.
   *
   * @var \Drupal\activitypub\Services\ActivityPubProcessClientInterface
   */
  protected $activityPubProcessClient;

  /**
   * SchedulerNodeSubscriber constructor
   *
   * @param \Drupal\activitypub\Services\ActivityPubProcessClientInterface $activitypub_process_client
   *   The ActivityPub process client service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(ActivityPubProcessClientInterface $activitypub_process_client, EntityTypeManagerInterface $entity_type_manager) {
    $this->activityStorage = $entity_type_manager->getStorage('activitypub_activity');
    $this->activityPubProcessClient = $activitypub_process_client;
  }

  /**
   * Queue to activity outbox on publish, if any.
   *
   * @param $event
   *   The scheduler event.
   */
  public function onSchedulerNodePublish($event) {
    $entity = $event->getEntity();

    $conditions = [
      'collection' => ActivityPubActivityInterface::OUTBOX,
      'entity_id' => $entity->id(),
      'entity_type_id' => 'node',
      'status' => 0
    ];

    foreach ($this->activityStorage->getActivities($conditions) as $activity) {
      $activity->setPublished();
      $this->activityPubProcessClient->createQueueItem($activity);
    }

    Cache::invalidateTags(['user:' . $entity->getOwnerId()]);
  }

  /**
   * Unpublish activities, if any.
   *
   * @param $event
   *   The scheduler event.
   */
  public function onSchedulerNodeUnpublish($event) {
    $entity = $event->getEntity();

    $conditions = [
      'collection' => ActivityPubActivityInterface::OUTBOX,
      'entity_id' => $entity->id(),
      'entity_type_id' => 'node',
      'status' => 1
    ];

    foreach ($this->activityStorage->getActivities($conditions) as $activity) {
      try {
        $activity->setUnpublished()->save();
      }
      catch (\Exception $ignored) {}
    }

    Cache::invalidateTags(['user:' . $entity->getOwnerId()]);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    if (class_exists('Drupal\scheduler\Event\SchedulerNodeEvents')) {
      $events[SchedulerNodeEvents::PUBLISH][] = ['onSchedulerNodePublish', 10];
      $events[SchedulerNodeEvents::UNPUBLISH][] = ['onSchedulerNodeUnpublish', 10];
      return $events;
    }

    return [];
  }

}
