<?php

namespace Drupal\activitypub\Entity;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of ActivityPub types.
 */
class ActivityPubTypeListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Label');
    $header['description'] = $this->t('Description');
    $header['status'] = $this->t('Status');
    $header['id'] = $this->t('Machine name');
    $header['plugin_id'] = $this->t('Plugin');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\activitypub\Entity\ActivityPubTypeInterface $entity */
    $row['label'] = $entity->label();
    $row['description'] = ['data' => ['#markup' => nl2br($entity->getDescription() ?: '')]];
    $row['status'] = $entity->status() ? $this->t('Enabled') : $this->t('Disabled');
    $row['id'] = $entity->id();
    $row['plugin_id'] = $this->getHumanPluginName($entity->getPlugin()['id']);
    return $row + parent::buildRow($entity);
  }

  /**
   * Get the Human name for a plugin.
   *
   * @param $id
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  protected function getHumanPluginName($id) {
    try {
      /** @var \Drupal\activitypub\Services\Type\TypePluginManager $plugin_manager */
      $plugin_manager = \Drupal::service('plugin.manager.activitypub.type');
      return $plugin_manager->getDefinition($id)['label'];
    }
    catch (\Exception $ignored) {
      return $id;
    }
  }

}
