<?php

namespace Drupal\activitypub\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the activitypub actor entity class.
 *
 * @ContentEntityType(
 *   id = "activitypub_actor",
 *   label = @Translation("ActivityPub actor"),
 *   label_collection = @Translation("ActivityPub actors"),
 *   label_singular = @Translation("ActivityPub actor"),
 *   label_plural = @Translation("ActivityPub actors"),
 *   label_count = @PluralTranslation(
 *     singular = "@count ActivityPub actor",
 *     plural = "@count ActivityPub actors",
 *   ),
 *   persistent_cache = FALSE,
 *   handlers = {
 *     "storage" = "Drupal\activitypub\Entity\Storage\ActivityPubActorStorage",
 *     "storage_schema" = "Drupal\activitypub\Entity\Storage\ActivityPubActorStorageSchema",
 *   },
 *   base_table = "activitypub_actor",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "id",
 *     "uuid" = "uuid",
 *     "owner" = "uid"
 *   }
 * )
 */
class ActivitypubActor extends ContentEntityBase implements ActivityPubActorInterface {

  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   *
   * When a new activitypub actor entity is created, set the uid entity
   * reference to the current user as the creator of the entity.
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += ['uid' => \Drupal::currentUser()->id()];
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetEntityId() {
    return $this->get('entity_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getType() {
    return $this->get('type')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    return $this->get('summary')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getBlockedDomains() {
    return $this->get('blocked_domains')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled() {
    return (bool) $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::ownerBaseFieldDefinitions($entity_type);

    $fields['entity_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Entity ID'))
      ->setRequired(TRUE);

    $fields['type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Type'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 128);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Username'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 128);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Status'))
      ->setDefaultValue(TRUE);

    $fields['summary'] = BaseFieldDefinition::create('string_long')
      ->setLabel('Summary');

    $fields['blocked_domains'] = BaseFieldDefinition::create('string_long')
      ->setLabel('Blocked domains');

    return $fields;
  }

}
