<?php

namespace Drupal\activitypub\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\views\EntityViewsData;

class ActivityPubActivityViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Activity bulk form.
    $data['activitypub_activity']['activity_bulk_form'] = array(
      'title' => $this->t('Activity operations bulk form'),
      'help' => $this->t('Add a form element that lets you run operations on multiple activities.'),
      'field' => array(
        'id' => 'activity_bulk_form',
      ),
    );

    // Change filter for type, collection, visibility and entity_type_id.
    $data['activitypub_activity']['type']['filter']['id'] = 'activitypub_type_select';
    $data['activitypub_activity']['collection']['filter']['id'] = 'activitypub_collection_select';
    $data['activitypub_activity']['visibility']['filter']['id'] = 'activitypub_visibility_select';
    $data['activitypub_activity']['entity_type_id']['filter']['id'] = 'activitypub_entity_type_id_select';
    $data['activitypub_activity']['config_id']['filter']['id'] = 'activitypub_config_id_select';

    // Provides an integration for each entity type except activitypub activity.
    foreach ($this->entityTypeManager->getDefinitions() as $entity_type_id => $entity_type) {
      if ($entity_type_id == 'activitypub_activity' || !$entity_type->entityClassImplements(ContentEntityInterface::class) || !$entity_type->getBaseTable()) {
        continue;
      }

      $label = $entity_type->getLabel();
      $base_table = $entity_type->getDataTable() ?: $entity_type->getBaseTable();
      $args = ['@entity_type' => $label];

      $data['activitypub_activity']['activitypub_' . $entity_type_id . '_id'] = [
        'title' => t('Activities on @entity_type', $args),
        'help' => t('Relate all activities on @entity_type.', $args),
        'relationship' => [
          'group' => t('activitypub'),
          'label' => t('ActivityPub'),
          'base' => $base_table,
          'base field' => $entity_type->getKey('id'),
          'relationship field' => 'entity_id',
          'id' => 'standard',
          'extra' => [
            [
              'table' => 'activitypub_activity',
              'field' => 'entity_type_id',
              'value' => $entity_type_id,
            ],
          ],
        ],
      ];
    }

    return $data;
  }

}