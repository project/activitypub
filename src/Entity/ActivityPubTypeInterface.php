<?php

namespace Drupal\activitypub\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining an activitypub type entity.
 */
interface ActivityPubTypeInterface extends ConfigEntityInterface {

  /**
   * Returns the description for this type.
   *
   * @return string
   */
  public function getDescription();

  /**
   * Returns whether the entity is locked or not.
   *
   * @return boolean
   */
  public function isLocked();

  /**
   * Returns whether the entity is enabled or not.
   *
   * @return boolean
   */
  public function isEnabled();

  /**
   * Returns the plugin definition.
   *
   * @return array
   *   An associative array with the following structure:
   *   - id: The type plugin id as a string.
   *   - config: The type configuration as an array.
   */
  public function getPlugin();

  /**
   * Sets the plugin definition.
   *
   * @param string $id
   *   The type plugin id.
   * @param array $configuration
   *   (optional) The type plugin configuration.
   */
  public function setPlugin($id, array $configuration = NULL);

}
