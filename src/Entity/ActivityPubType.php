<?php

namespace Drupal\activitypub\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Defines the activitypub type entity.
 *
 * @ConfigEntityType(
 *   id = "activitypub_type",
 *   label = @Translation("ActivityPub type"),
 *   label_collection = @Translation("ActivityPub type"),
 *   label_singular = @Translation("ActivityPub type"),
 *   label_plural = @Translation("ActivityPub types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count ActivityPub type",
 *     plural = "@count ActivityPub types",
 *   ),
 *   handlers = {
 *     "access" = "Drupal\activitypub\Entity\ActivityPubTypeAccessControlHandler",
 *     "list_builder" = "Drupal\activitypub\Entity\ActivityPubTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\activitypub\Form\ActivityPubTypeForm",
 *       "edit" = "Drupal\activitypub\Form\ActivityPubTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     }
 *   },
 *   config_prefix = "activitypub_type",
 *   admin_permission = "administer activitypub types",
 *   links = {
 *     "collection" = "/admin/config/services/activitypub/activitypub-type",
 *     "add-form" = "/admin/config/services/activitypub/activitypub-type/add",
 *     "edit-form" = "/admin/config/services/activitypub/activitypub-type/{activitypub_type}",
 *     "delete-form" = "/admin/config/services/activitypub/activitypub-type/{activitypub_type}/delete",
 *     "enable" = "/admin/config/services/activitypub/activitypub-type/{activitypub_type}/enable",
 *     "disable" = "/admin/config/services/activitypub/activitypub-type/{activitypub_type}/disable"
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "status" = "status"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "locked",
 *     "plugin"
 *   }
 * )
 */
class ActivityPubType extends ConfigEntityBase implements ActivityPubTypeInterface {

  /**
   * The ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The label.
   *
   * @var string
   */
  protected $label;

  /**
   * The description.
   *
   * @var string
   */
  protected $description;

  /**
   * The status.
   *
   * @var bool
   */
  protected $status = TRUE;

  /**
   * The status.
   *
   * @var bool
   */
  protected $locked = FALSE;

  /**
   * The plugin definition.
   *
   * @var array
   */
  protected $plugin;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function isLocked() {
    return $this->locked;
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled() {
    return $this->status;
  }

  /**
   * {@inheritdoc}
   */
  public function getPlugin() {
    return $this->plugin;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    parent::calculateDependencies();

    /** @var \Drupal\activitypub\Services\Type\TypePluginInterface $object */
    $configuration = [];
    if (isset($this->getPlugin()['configuration'])) {
      $configuration = $this->getPlugin()['configuration'];
    }
    $object = $this->getTypePluginManager()->createInstance($this->getPlugin()['id'], $configuration);
    $dependencies = $object->calculateDependencies();
    if (!empty($dependencies)) {
      $this->addDependencies($dependencies);
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setPlugin($id, array $configuration = NULL) {
    if ($configuration === NULL) {
      $instance = $this->getTypePluginManager()->createInstance($id);
      // Get the default configuration for this plugin.
      $configuration = $instance->getConfiguration();
    }
    $this->plugin = ['id' => $id, 'configuration' => $configuration];
  }

  /**
   * {@inheritdoc}
   */
  public static function sort(ConfigEntityInterface $a, ConfigEntityInterface $b) {
    if ($a->isLocked() == $b->isLocked()) {
      $a_label = $a->label();
      $b_label = $b->label();
      return strnatcasecmp($a_label, $b_label);
    }
    return $a->isLocked() ? 1 : -1;
  }

  /**
   * Returns the object plugin manager.
   *
   * @return \Drupal\activitypub\Services\Type\TypePluginManager
   */
  protected function getTypePluginManager() {
    if (!isset($this->typePluginManager)) {
      $this->typePluginManager = \Drupal::service('plugin.manager.activitypub.type');
    }
    return $this->typePluginManager;
  }
}
