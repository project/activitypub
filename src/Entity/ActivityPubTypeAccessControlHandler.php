<?php

namespace Drupal\activitypub\Entity;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access control handler for the activitypub type entity.
 */
class ActivityPubTypeAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected $viewLabelOperation = TRUE;

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    // There are no restrictions on viewing the label.
    if ($operation === 'view label') {
      return AccessResult::allowed();
    }
    // Locked entities cannot be updated or deleted.
    elseif (in_array($operation, ['update', 'delete'])) {
      $can_edit = FALSE;
      if ($operation == 'update' && $entity->id() == 'inbox_reply') {
        $can_edit = TRUE;
      }
      if ($entity->isLocked() && !$can_edit) {
        return AccessResult::forbidden('The config entity is locked.')->addCacheableDependency($entity);
      }
      else {
        return parent::checkAccess($entity, $operation, $account)->addCacheableDependency($entity);
      }
    }

    return parent::checkAccess($entity, $operation, $account);
  }

}
