<?php

namespace Drupal\activitypub\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining an activitypub activity entity type.
 */
interface ActivityPubActivityInterface extends ContentEntityInterface, EntityOwnerInterface, EntityPublishedInterface {

  // Collections constants.
  const FOLLOWERS = 'followers';
  const FOLLOWING = 'following';
  const INBOX = 'inbox';
  const OUTBOX = 'outbox';

  // Activity URL constants.
  const CONTEXT_URL = 'https://www.w3.org/ns/activitystreams';
  const SECURITY_URL = 'https://w3id.org/security/v1';
  const PUBLIC_URL = 'https://www.w3.org/ns/activitystreams#Public';

  // Visibility constants.
  const VISIBILITY_PUBLIC = 1;
  const VISIBILITY_FOLLOWERS = 2;
  const VISIBILITY_UNLISTED = 3;
  const VISIBILITY_PRIVATE = 4;

  /**
   * Returns the object plugin manager.
   *
   * @return \Drupal\activitypub\Services\Type\TypePluginManager
   */
  public function getTypePluginManager();

  /**
   * Gets the activity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the activity.
   */
  public function getCreatedTime();

  /**
   * Returns whether the activity is processed or not.
   *
   * @return boolean
   */
  public function isProcessed();

  /**
   * Returns whether the activity is queued or not.
   *
   * @return boolean
   */
  public function isQueued();

  /**
   * Returns the collection.
   *
   * @return string
   */
  public function getCollection();

  /**
   * Returns the external ID.
   *
   * @return string
   */
  public function getExternalId();

  /**
   * Returns the config id.
   *
   * @return string
   */
  public function getConfigID();

  /**
   * Returns the Activity type.
   *
   * @return string
   */
  public function getType();

  /**
   * Returns the object.
   *
   * @return string
   */
  public function getObject();

  /**
   * Returns the reply.
   *
   * @return string
   */
  public function getReply();

  /**
   * Returns the Activity actor.
   *
   * @return string
   */
  public function getActor();

  /**
   * Returns the target Entity Type.
   *
   * @return string
   */
  public function getTargetEntityTypeId();

  /**
   * Return the target entity id.
   *
   * @return integer
   */
  public function getTargetEntityId();

  /**
   * Get the payload.
   *
   * @return string
   */
  public function getPayLoad();

  /**
   * Get the context.
   *
   * @return string
   */
  public function getContext();

  /**
   * Get array of URL's for the to property.
   *
   * @return array
   */
  public function getTo();

  /**
   * Get visibility.
   *
   * @return integer
   */
  public function getVisibility();

  /**
   * Returns whether the item was read or not.
   *
   * @return bool
   */
  public function isRead();

  /**
   * Returns whether the item is muted or not.
   *
   * @return bool
   */
  public function isMuted();

  /**
   * Returns whether the item is public or not.
   *
   * @return bool
   */
  public function isPublic();

  /**
   * Returns whether the item is for followers only or not.
   *
   * @return bool
   */
  public function isFollowers();

  /**
   * Returns whether the item is unlisted or not.
   *
   * @return bool
   */
  public function isUnlisted();

  /**
   * Returns whether the item is private or not.
   *
   * @return bool
   */
  public function isPrivate();

  /**
   * Whether this activity can be sent to a shared inbox.
   *
   * @return bool
   */
  public function canUseSharedInbox();

  /**
   * Set to audience.
   *
   * @param $to
   *
   * @return $this
   */
  public function setTo($to);

  /**
   * Set the visibility
   *
   * @param int $visibility
   *
   * @return $this
   */
  public function setVisibility(int $visibility);

  /**
   * Set the activity visibility to public.
   *
   * @return $this
   */
  public function setPublic();

  /**
   * Set the activity visibility to followers only.
   *
   * @return $this
   */
  public function setFollowers();

  /**
   * Set the activity visibility to unlisted.
   *
   * @return $this
   */
  public function setUnlisted();

  /**
   * Set the activity visibility to private.
   *
   * @return $this
   */
  public function setPrivate();

  /**
   * Mute an activity.
   *
   * @return $this
   */
  public function mute();

  /**
   * Unmute an activity.
   *
   * @return $this
   */
  public function unMute();

  /**
   * Get the raw to value.
   *
   * @return string
   */
  public function getToRaw();

  /**
   * Build the activity.
   *
   * @return array
   */
  public function buildActivity();

  /**
   * Inbox pre save call.
   *
   * @param $doSave
   *   Whether to save the activity, defaults to TRUE.
   */
  public function preInboxSave(&$doSave);

  /**
   * Outbox pre save call.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity being saved, usually a node.
   * @param \Drupal\activitypub\Entity\ActivityPubTypeInterface $activityPubType
   *   The activitypub type.
   * @param $doSave
   *   Whether to save the activity, defaults to TRUE.
   */
  public function preOutboxSave(EntityInterface $entity, ActivityPubTypeInterface $activityPubType, &$doSave);

  /**
   * Act on entity delete.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity that is being deleted. It is a content entity, apart from the
   *   user or activitypub actor.
   */
  public function onEntityDelete(EntityInterface $entity);

  /**
   * Do inbox processing.
   *
   * @param null $type
   *   The activity pub type.
   */
  public function doInboxProcess($type = NULL);

  /**
   * Returns whether the activity can be queued.
   *
   * @return bool
   */
  public function canBeQueued();

  /**
   * Returns whether the activity can be undone.
   *
   * @return bool
   */
  public function canBeUndone();

  /**
   * Returns whether a type is enabled or not.
   *
   * @param $type
   *
   * @return bool
   */
  public function typeIsEnabled($type);

}
