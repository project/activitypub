<?php

namespace Drupal\activitypub\Entity\Storage;

use Drupal\Core\Entity\ContentEntityStorageInterface;

/**
 * Defines an interface for ActivityPub activity entity storage class.
 */
interface ActivityPubActivityStorageInterface extends ContentEntityStorageInterface {

  /**
   * Get activities.
   *
   * @param array $conditions
   * @param array $sort
   * @param int $limit
   *
   * @return \Drupal\activitypub\Entity\ActivityPubActivityInterface[]
   */
  public function getActivities($conditions = [], array $sort = [], int $limit = 0);

  /**
   * Get activity records.
   *
   * @param array $conditions
   *
   * @return array $records
   */
  public function getActivityRecords($conditions = []);

  /**
   * Get activity count.
   *
   * @param array $conditions
   *
   * @return int
   */
  public function getActivityCount($conditions = []);

  /**
   * Chang read status in inbox.
   *
   * @param $status
   *   The status to update to.
   * @param $id
   *   The channel id.
   * @param array $items
   *   The items to update, if any.
   */
  public function changeReadStatus($status, $id, $items = []);

  /**
   * Get options for a views filter.
   *
   * @param $field
   *
   * @return array $options
   */
  public function getFieldOptions($field);

}
