<?php

namespace Drupal\activitypub\Entity\Storage;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;

/**
 * Storage class for ActivityPub actors.
 */
class ActivityPubActorStorage extends SqlContentEntityStorage implements ActivityPubActorStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function getActorCount($conditions = []) {
    $query = $this->database->select($this->getBaseTable(), 't');
    $query->fields('t', ['id']);
    foreach ($conditions as $key => $value) {
      $operator = is_array($value) ? 'IN' : '=';
      if (strpos($key, '!') !== FALSE) {
        $operator = is_array($value) ? 'NOT IN' : '<>';
        $key = str_replace('!', '', $key);
      }
      $query->condition($key, $value, $operator);
    }
    return $query->countQuery()->execute()->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function loadActorByEntityIdAndType($entity_id, $type) {
    $entities = $this->loadByProperties(['entity_id' => $entity_id, 'type' => $type]);
    if ($entities) {
      return array_shift($entities);
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function loadActorByName($name) {
    $entities = $this->loadByProperties(['name' => $name]);
    if ($entities) {
      return array_shift($entities);
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function nameExists($name) {
    return (bool) $this->database->select($this->getBaseTable(), 'a')
      ->fields('a', ['id'])
      ->condition('name', $name)
      ->execute()
      ->fetchField();
  }

}
