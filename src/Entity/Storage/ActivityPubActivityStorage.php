<?php

namespace Drupal\activitypub\Entity\Storage;

use Drupal\activitypub\Entity\ActivityPubActivityInterface;
use Drupal\Core\Database\Query\PagerSelectExtender;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;

/**
 * Storage class for ActivityPub activities.
 */
class ActivityPubActivityStorage extends SqlContentEntityStorage implements ActivityPubActivityStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function getActivities($conditions = [], $sort = [], int $limit = 0) {
    $query = $this->getBaseQuery($conditions, $sort, $limit);
    $ids = $query->execute()->fetchAllKeyed(0, 0);
    return $this->loadMultiple($ids);
  }

  /**
   * {@inheritdoc}
   */
  public function getActivityRecords($conditions = []) {
    $query = $this->getBaseQuery($conditions);
    return $query->execute()->fetchAll();
  }

  /**
   * {@inheritdoc}
   */
  public function getActivityCount($conditions = []) {
    $query = $this->getBaseQuery($conditions);
    return (int) $query->countQuery()->execute()->fetchField();
  }

  /**
   * Return a select query.
   *
   * @param array $conditions
   * @param array $sort
   * @param int $limit
   *
   * @return \Drupal\Core\Database\Query\SelectInterface
   */
  protected function getBaseQuery(array $conditions = [], array $sort = [], int $limit = 0) {

    if ($limit) {
      $query = $this->database->select($this->getBaseTable(), 't')
        ->extend(PagerSelectExtender::class)->limit($limit);
    }
    else {
      $query = $this->database->select($this->getBaseTable(), 't');
    }

    $query->fields('t', ['id', 'actor', 'object', 'status', 'mute']);
    foreach ($conditions as $field => $value) {
      $operator = is_array($value) ? 'IN' : '=';
      if (strpos($field, '!') !== FALSE) {
        $operator = is_array($value) ? 'NOT IN' : '<>';
        $field = str_replace('!', '', $field);
      }
      if (strpos($field, '<') !== FALSE) {
        $operator = '<';
        $field = str_replace('<', '', $field);
      }
      if ($value === 'isNull') {
        $query->isNull($field);
      }
      elseif ($value === 'isNotNull') {
        $query->isNotNull($field);
      }
      elseif ($field == 'search') {
        $query->condition('payload', '%' . $this->database->escapeLike($value) . '%', 'LIKE');
      }
      else {
        $query->condition($field, $value, $operator);
      }
    }

    if (!empty($sort)) {
      foreach ($sort as $s) {
        $query->orderBy($s[0], $s[1]);
      }
    }

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function changeReadStatus($status, $id, $items = []) {
    $is_read_condition = $status ? 0 : 1;
    $query = $this->database->update('activitypub_activity')
      ->fields(['is_read' => $status])
      ->condition('is_read', $is_read_condition);

    if ($id == 'direct') {
      $query->condition('visibility', ActivityPubActivityInterface::VISIBILITY_PRIVATE);
    }

    if ($id == 'home') {
      $query->condition('type', ['Create', 'Like', 'Announce'], 'IN');
      $query->isNull('entity_id');
    }

    if ($id == 'notifications') {
      $query->condition('type', ['Like', 'Announce', 'Follow', 'Create'], 'IN');
      $query->isNotNull('entity_id');
    }

    if (!empty($items)) {
      $query->condition('id', $items, 'IN');
    }

    $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldOptions($field) {
    return $this->database->select('activitypub_activity', 'aa')
      ->fields('aa', [$field])
      ->isNotNull($field)
      ->distinct()
      ->orderBy($field)
      ->execute()
      ->fetchAllKeyed(0, 0);
  }

}
