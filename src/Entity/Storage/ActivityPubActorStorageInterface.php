<?php

namespace Drupal\activitypub\Entity\Storage;

use Drupal\Core\Entity\ContentEntityStorageInterface;

/**
 * Defines an interface for ActivityPub Actor entity storage class.
 */
interface ActivityPubActorStorageInterface extends ContentEntityStorageInterface {

  /**
   * Get actor count.
   *
   * @param array $conditions
   *
   * @return int
   */
  public function getActorCount($conditions = []);

  /**
   * Load an actor by entity id and type.
   *
   * @param $entity_id
   * @param $type
   *
   * @return NULL|\Drupal\activitypub\Entity\ActivitypubActorInterface
   */
  public function loadActorByEntityIdAndType($entity_id, $type);

  /**
   * Load an actor by name.
   *
   * @param $name
   *
   * @return NULL|\Drupal\activitypub\Entity\ActivitypubActorInterface
   */
  public function loadActorByName($name);

  /**
   * Checks whether a name exists or not.
   *
   * @param $name
   *
   * @return boolean
   */
  public function nameExists($name);

}
