<?php

namespace Drupal\activitypub\Entity;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a listing of ActivityPub activities.
 */
class ActivityPubActivityListBuilder extends EntityListBuilder {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Constructs a new PathAliasListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, AccountInterface $current_user, DateFormatterInterface $date_formatter) {
    parent::__construct($entity_type, $storage);
    $this->currentUser = $current_user;
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('current_user'),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('ID');
    $header['created'] = $this->t('Created');
    $header['collection'] = $this->t('Collection');
    $header['config_id'] = $this->t('Config ID');
    $header['type'] = $this->t('Type');
    $header['actor'] = $this->t('Actor');
    $header['object'] = $this->t('Object');
    $header['queued'] = $this->t('Queued');
    $header['processed'] = $this->t('Processed');
    $header['status'] = $this->t('Status');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\activitypub\Entity\ActivityPubActivityInterface $entity */
    $row['id'] = $entity->id();
    $row['created'] = $entity->getCreatedTime() ? $this->dateFormatter->format($entity->getCreatedTime(), 'short') : '/';
    $row['collection'] = $entity->getCollection();
    $row['config_id'] = $entity->getConfigID();
    $row['type'] = $entity->getType();
    $row['actor'] = $entity->getActor();
    $row['object'] = $entity->getObject();
    $row['queued'] = $entity->isQueued() ? $this->t('Yes') : $this->t('No');
    $row['processed'] = $entity->isProcessed() ? $this->t('Yes') : $this->t('No');
    $row['status'] = $entity->isPublished() ? $this->t('Published') : $this->t('Unpublished');
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityIds() {
    // Sort descending default.
    $query = $this->getStorage()->getQuery()
      ->accessCheck()
      ->condition('uid', $this->currentUser->id())
      ->sort($this->entityType->getKey('id'), 'DESC');

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    /** @var \Drupal\activitypub\Entity\ActivityPubActivityInterface $entity */
    $operations = parent::getDefaultOperations($entity);

    if ($entity->canBeQueued() && $entity->access('queue')) {
      $operations['queue'] = [
        'title' => $this->t('Add to queue'),
        'weight' => 20,
        'url' => $this->ensureDestination($entity->toUrl('queue')),
      ];
    }

    if ($entity->canBeUndone() && $entity->access('undo')) {
      $operations['undo'] = [
        'title' => $this->t('Undo'),
        'weight' => 20,
        'url' => $this->ensureDestination($entity->toUrl('undo')),
      ];
    }

    return $operations;
  }

}
