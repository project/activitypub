<?php

namespace Drupal\activitypub\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining an activitypub actor entity type.
 */
interface ActivityPubActorInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * Returns the entity id.
   */
  public function getTargetEntityId();

  /**
   * Returns the type.
   */
  public function getType();

  /**
   * Returns the name of the Actor.
   *
   * @return string
   */
  public function getName();

  /**
   * Returns the summary of the Actor.
   *
   * @return string
   */
  public function getSummary();

  /**
   * Returns the blocked domains of the Actor.
   *
   * @return string
   */
  public function getBlockedDomains();

  /**
   * Returns whether the actor type is enabled or not.
   *
   * @return boolean
   */
  public function isEnabled();

}
