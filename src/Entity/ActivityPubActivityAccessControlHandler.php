<?php

namespace Drupal\activitypub\Entity;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityHandlerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Access control handler for the ActivityPub Activity entity.
 */
class ActivityPubActivityAccessControlHandler extends EntityAccessControlHandler implements EntityHandlerInterface{

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')
    );
  }

  /**
   * Constructs the block access control handler instance
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($entity_type);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {

    /** @var \Drupal\activitypub\Entity\ActivityPubActivityInterface $entity */
    switch ($operation) {
      case 'view':
        return AccessResult::allowed();

      case 'queue':
      case 'undo':
      case 'update':
      case 'delete':
        return AccessResult::allowedIf($account->id() == $entity->getOwnerId() || $account->hasPermission('manage all activitypub activities'));
    }

    // Unknown operation, just forbid.
    return AccessResult::forbidden();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    /** @var \Drupal\activitypub\Entity\Storage\ActivityPubActorStorageInterface $storage */
    $storage = $this->entityTypeManager->getStorage('activitypub_actor');
    $actor = $storage->loadActorByEntityIdAndType($account->id(), 'person');
    if ($actor) {
      return AccessResult::allowedIfHasPermission($account, 'allow users to enable activitypub');
    }

    return AccessResult::forbidden();
  }

}
