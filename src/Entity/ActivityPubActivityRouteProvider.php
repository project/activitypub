<?php

namespace Drupal\activitypub\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\EntityRouteProviderInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Provides routes for ActivityPub Activities.
 */
class ActivityPubActivityRouteProvider implements EntityRouteProviderInterface {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $route_collection = new RouteCollection();
    $route = (new Route('/activitypub/{activitypub_activity}'))
      ->addDefaults([
        '_controller' => '\Drupal\activitypub\Controller\ActivityController::view',
      ])
      ->setRequirement('activitypub_activity', '\d+')
      ->setRequirement('_permission', 'access content');
    $route_collection->add('entity.activitypub_activity.canonical', $route);

    $route = (new Route('/activitypub/add'))
      ->setDefault('_entity_form', 'activitypub_activity.add')
      ->setDefault('entity_type_id', 'activitypub_activity')
      ->setRequirement('_entity_create_access', 'activitypub_activity')
      ->setOption('_admin_route', TRUE);
    $route_collection->add('entity.activitypub_activity.add_form', $route);

    $route = (new Route('/activitypub/add/follow'))
      ->setDefault('_entity_form', 'activitypub_activity.add')
      ->setDefault('entity_type_id', 'activitypub_activity')
      ->setRequirement('_entity_create_access', 'activitypub_activity')
      ->setOption('_admin_route', TRUE);
    $route_collection->add('entity.activitypub_activity.add_form_follow', $route);

    $route = (new Route('/activitypub/{activitypub_activity}/edit'))
      ->setDefault('_entity_form', 'activitypub_activity.edit')
      ->setRequirement('_entity_access', 'activitypub_activity.update')
      ->setRequirement('activitypub_activity', '\d+')
      ->setOption('_admin_route', TRUE);
    $route_collection->add('entity.activitypub_activity.edit_form', $route);

    $route = (new Route('/activitypub/{activitypub_activity}/queue'))
      ->setDefaults([
        '_controller' => '\Drupal\activitypub\Controller\ActivityController::queue',
        '_title' => 'Queue Activity',
      ])
      ->setRequirement('_entity_access', 'activitypub_activity.queue')
      ->setRequirement('activitypub_activity', '\d+')
      ->setOption('_admin_route', TRUE);
    $route_collection->add('entity.activitypub_activity.queue', $route);

    $route = (new Route('/activitypub/{activitypub_activity}/undo'))
      ->setDefaults([
        '_controller' => '\Drupal\activitypub\Controller\ActivityController::undo',
        '_title' => 'Undo Activity',
      ])
      ->setRequirement('_entity_access', 'activitypub_activity.undo')
      ->setRequirement('activitypub_activity', '\d+')
      ->setOption('_admin_route', TRUE);
    $route_collection->add('entity.activitypub_activity.undo', $route);

    $route = (new Route('/activitypub/{activitypub_activity}/delete'))
      ->setDefault('_entity_form', 'activitypub_activity.delete')
      ->setRequirement('_entity_access', 'activitypub_activity.delete')
      ->setRequirement('activitypub_activity', '\d+')
      ->setOption('_admin_route', TRUE);
    $route_collection->add('entity.activitypub_activity.delete_form', $route);

    return $route_collection;
  }

}
