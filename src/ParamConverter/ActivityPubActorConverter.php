<?php

namespace Drupal\activitypub\ParamConverter;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\ParamConverter\ParamConverterInterface;
use Symfony\Component\Routing\Route;

class ActivityPubActorConverter implements ParamConverterInterface {

  /**
   * The ActivityPub Actor storage.
   *
   * @var \Drupal\activitypub\Entity\Storage\ActivityPubActorStorageInterface
   */
  protected $actorStorage;

  /**
   * ActivityPubActorConverter constructor
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    try {
      $this->actorStorage = $entity_type_manager->getStorage('activitypub_actor');
    }
    catch (InvalidPluginDefinitionException $ignored) {}
    catch (PluginNotFoundException $ignored) {}
  }

  /**
   * {@inheritdoc}
   */
  public function convert($value, $definition, $name, array $defaults) {
    if ($this->actorStorage) {
      return $this->actorStorage->loadActorByName($value);
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function applies($definition, $name, Route $route) {
    if (!empty($definition['type']) && $definition['type'] == 'activitypub_actor') {
      return TRUE;
    }
    return FALSE;
  }


}
