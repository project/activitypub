<?php

namespace Drupal\activitypub\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a ActivityPub type annotation.
 *
 * @see \Drupal\activitypub\Services\Type\TypePluginManager
 * @see plugin_api
 *
 * @ingroup plugin_api
 *
 * @Annotation
 */
class ActivityPubType extends Plugin {

  /**
   * The plugin id.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The weight of the plugin.
   *
   * @var int
   */
  public $weight;

}
