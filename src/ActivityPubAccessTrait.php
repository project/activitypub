<?php

namespace Drupal\activitypub;

use Drupal\activitypub\Entity\ActivityPubActorInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\user\UserInterface;

trait ActivityPubAccessTrait {

  /**
   * Check that the actor has enabled activitypub.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result for this method.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function enabledActorCheck() {
    /** @var \Drupal\activitypub\Entity\Storage\ActivityPubActorStorageInterface $storage */
    $actor_storage = \Drupal::entityTypeManager()->getStorage('activitypub_actor');
    $actor = $actor_storage->loadActorByEntityIdAndType($this->currentUser()->id(), 'person');

    if ($actor) {
      return AccessResult::allowed();
    }

    return AccessResult::forbidden();
  }

  /**
   * Check that the actor points to the user.
   *
   * @param \Drupal\user\UserInterface $user
   *
   * @param \Drupal\activitypub\Entity\ActivityPubActorInterface $activitypub_actor
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result for this method.
   */
  public function actorUserCheck(UserInterface $user, ActivityPubActorInterface $activitypub_actor) {

    if ($user->id() == $activitypub_actor->getTargetEntityId()) {
      return AccessResult::allowed();
    }

    return AccessResult::forbidden();
  }

  /**
   * Check that the actor points to the user.
   *
   * @param \Drupal\user\UserInterface $user
   * @param \Drupal\activitypub\Entity\ActivityPubActorInterface $activitypub_actor
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result for this method.
   */
  public function actorFollowUserCheck(UserInterface $user, ActivityPubActorInterface $activitypub_actor) {
    return $this->actorUserCheck($user, $activitypub_actor);
  }


}