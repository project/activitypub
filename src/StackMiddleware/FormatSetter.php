<?php

namespace Drupal\activitypub\StackMiddleware;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Set json request form based on headers used in federation.
 */
class FormatSetter implements HttpKernelInterface {

  /**
   * The wrapped HTTP kernel.
   *
   * @var \Symfony\Component\HttpKernel\HttpKernelInterface
   */
  protected $httpKernel;

  /**
   * Constructs a FormatSetter object.
   *
   * @param \Symfony\Component\HttpKernel\HttpKernelInterface $http_kernel
   *   The decorated kernel.
   */
  public function __construct(HttpKernelInterface $http_kernel) {
    $this->httpKernel = $http_kernel;
  }

  /**
   * {@inheritdoc}
   */
  public function handle(Request $request, $type = self::MAIN_REQUEST, $catch = true): Response {
    if ($request->headers->has('Accept')) {
      $accept = $request->headers->get('Accept');
      if (str_contains($accept, "application/activity+json") || str_contains($accept, 'application/ld+json; profile="https://www.w3.org/ns/activitystreams"')) {
        // @see https://www.drupal.org/project/activitypub/issues/3272176 why
        // we are not using 'json'.
        $request->setRequestFormat('activity_json');
      }
    }

    return $this->httpKernel->handle($request, $type, $catch);
  }

}
