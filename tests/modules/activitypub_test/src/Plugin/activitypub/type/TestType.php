<?php

namespace Drupal\activitypub_test\Plugin\activitypub\type;

use Drupal\activitypub\Entity\ActivityPubActivityInterface;
use Drupal\activitypub\Services\Type\TypePluginBase;
use Drupal\Core\Entity\EntityInterface;

/**
 * The ActivityPub test plugin.
 *
 * @ActivityPubType(
 *   id = "activitypub_test",
 *   label = @Translation("Test plugin")
 * )
 */
class TestType extends TypePluginBase {

  /**
   * {@inheritdoc}
   */
  public function build(ActivityPubActivityInterface $activity, EntityInterface $entity = NULL) {
    $to = $cc = $mention = [];
    $this->buildAudience($to, $cc, $mention, $activity);

    $return = [
      'type' => 'test',
      'id' => 'testId',
      'actor' => 'actor',
      'to' => $to,
      'cc' => $cc,
    ];

    if (!empty($mention)) {
      $return['object']['tag'] = [(object) $mention];
    }

    return $return;
  }

}