<?php

namespace Drupal\Tests\activitypub\Functional;

use Drupal\activitypub\Entity\ActivityPubActivityInterface;
use Drupal\Core\Url;

/**
 * Tests context functionality.
 *
 * Note: reply context is tested in ReaderTest for instance.
 *
 * @group activitypub
 */
class ContextTest extends ActivityPubTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'link',
    'field_ui',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create page type.
    $this->createType('Create', 'page', 'Note', [], 'article');

    // Create like (content) type.
    $mapping = ['field_link' => 'object'];
    $this->createType('Like', 'like', '', $mapping, 'like');

    $this->drupalAddLinkField('like');
    drupal_flush_all_caches();
  }

  /**
   * Tests context.
   */
  public function testContext() {
    $assert_session = $this->assertSession();

    /** @var \Drupal\activitypub\Entity\Storage\ActivityPubActorStorageInterface $storage */
    $storage = \Drupal::entityTypeManager()->getStorage('activitypub_activity');

    // Setup ActivityPub actor.
    $this->enableActivityPub($assert_session, 2);
    $this->drupalLogout();
    $actor_href_1 = Url::fromRoute('activitypub.user.self', ['user' => $this->authenticatedUserOne->id(), 'activitypub_actor' => $this->accountNameOne], ['absolute' => TRUE])->toString();
    $actor_href_2 = Url::fromRoute('activitypub.user.self', ['user' => $this->authenticatedUserTwo->id(), 'activitypub_actor' => $this->accountNameTwo], ['absolute' => TRUE])->toString();

    // Follow the user, so the followee check runs since we don't sign.
    $this->followUserProgrammatically($this->authenticatedUserOne->id(), $actor_href_2, $actor_href_1);

    // Set inbox and outbox handler.
    $this->setOutboxHandler();
    $this->setInboxHandler();

    // Enable context type.
    $this->setTypeStatus('context');

    // Create a node.
    $this->drupalLogin($this->authenticatedUserOne);
    $this->drupalGet('node/add/page');
    $edit = [
      'title[0][value]' => 'Hello Awesome content!',
      'body[0][value]' => 'The best page in the world!',
      'activitypub_create' => 'article',
      'status[value]' => TRUE,
    ];
    $this->submitForm($edit, 'Save');
    $node = \Drupal::entityTypeManager()->getStorage('node')->load(1);
    $node_url = $node->toUrl('canonical', ['absolute' => TRUE])->toString();
    $this->drupalLogout();

    // No need to send out.
    $this->clearQueue(ACTIVITYPUB_OUTBOX_QUEUE);

    // Send a like to outbox.
    $this->drupalLogin($this->authenticatedUserTwo);
    $this->drupalGet('node/add/like');
    $edit = [
      'title[0][value]' => 'Hello like!',
      'activitypub_create' => 'like',
      'activitypub_to' => $actor_href_1,
      'field_link[0][uri]' => $node_url,
      'status[value]' => TRUE,
    ];
    $this->submitForm($edit, 'Save');
    $this->drupalLogout();

    $count = \Drupal::queue(ACTIVITYPUB_OUTBOX_QUEUE)->numberOfItems();
    self::assertEquals(1, $count);
    $count = \Drupal::queue(ACTIVITYPUB_INBOX_QUEUE)->numberOfItems();
    self::assertEquals(0, $count);

    // Run outbox queue.
    $this->runOutboxQueue();
    $count = \Drupal::queue(ACTIVITYPUB_OUTBOX_QUEUE)->numberOfItems();
    self::assertEquals(0, $count);
    $count = \Drupal::queue(ACTIVITYPUB_INBOX_QUEUE)->numberOfItems();
    self::assertEquals(1, $count);

    /** @var \Drupal\activitypub\Entity\ActivityPubActivityInterface $activity2 */
    /** @var \Drupal\activitypub\Entity\ActivityPubActivityInterface $activity3 */
    /** @var \Drupal\activitypub\Entity\ActivityPubActivityInterface $activity4 */
    /** @var \Drupal\activitypub\Entity\ActivityPubActivityInterface $activity5 */

    $activity2 = $storage->load(2);
    self::assertEquals('article', $activity2->getConfigID());
    self::assertEquals('outbox', $activity2->getCollection());
    $activity3 = $storage->load(3);
    self::assertEquals('like', $activity3->getConfigID());
    self::assertEquals('outbox', $activity3->getCollection());
    $activity4 = $storage->load(4);
    self::assertEquals('Like', $activity4->getType());
    self::assertEquals('inbox', $activity4->getCollection());
    self::assertEquals($node_url, $activity4->getObject());
    self::assertEquals($this->authenticatedUserOne->id(), $activity4->getOwnerId());

    // We used to create an additional context activity stored in the 'Context'
    // collection. That doesn't happen anymore, so this test simply verifies
    // there's no fourth activity.
    $activity5 = $storage->load(5);
    self::assertNull($activity5);

    // Run inbox queue and verify the context of activity 4 is not empty and
    // contains the content of node 1.
    $this->runInboxQueue();
    $count = \Drupal::queue(ACTIVITYPUB_INBOX_QUEUE)->numberOfItems();
    self::assertEquals(0, $count);

    /** @var \Drupal\activitypub\Entity\ActivityPubActivityInterface $activity4 */
    $storage->resetCache([4]);
    $activity4 = $storage->load(4);
    self::assertEquals('Like', $activity4->getType());
    self::assertEquals('inbox', $activity4->getCollection());
    $context = @json_decode($activity4->getContext(), TRUE);
    self::assertNotEmpty($context);
    self::assertEquals('<p>' . $node->get('body')->value . '</p>', $context['content']);
  }

}
