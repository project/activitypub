<?php

namespace Drupal\Tests\activitypub\Functional;

/**
 * Tests Follow functionality with language.
 *
 * @group activitypub
 */
class FollowLocalizedTest extends FollowTest {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'language',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->setupLanguage('es');
  }

  /**
   * Test follow functionality.
   */
  public function testFollow() {
    $this->testFollowHelper();
  }

}
