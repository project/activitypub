<?php

namespace Drupal\Tests\activitypub\Functional;

/**
 * Tests (text) format functionality.
 *
 * @group activitypub
 */
class FormatTest extends ActivityPubTestBase {

  /**
   * Tests that a request with the activity/json header falls back to HTML when
   * text/html is in the accept header.
   */
  public function testRequestFallbackFormat() {
    $assert_session = $this->assertSession();

    $this->enableActivityPub($assert_session);
    $this->drupalLogout();

    // Create type.
    $this->createType('Create', 'page', 'Note');

    // Send to outbox.
    $this->drupalLogin($this->authenticatedUserOne);
    $this->drupalGet('node/add/page');
    $edit = [
      'title[0][value]' => 'Page one',
      'body[0][value]' => 'Hello AP world!',
      'activitypub_create' => 'map',
    ];
    $this->submitForm($edit, 'Save');

    $this->drupalGet('node/add/page');
    $edit = [
      'title[0][value]' => 'Page two',
      'body[0][value]' => 'Hello HTML world!',
    ];
    $this->submitForm($edit, 'Save');
    $this->drupalLogout();

    $this->drupalGet('node/1', [], ['Accept' => 'application/activity+json, text/html']);
    $assert_session->responseHeaderContains('content-type', 'application/activity+json');

    $this->drupalGet('node/2', [], ['Accept' => 'application/activity+json, text/html; charset=UTF-8']);
    $assert_session->responseHeaderContains('content-type', 'text/html; charset=UTF-8');

    $this->drupalGet('node/2', [], ['Accept' => 'application/activity+json']);
    $assert_session->statusCodeEquals(404);
  }

}
