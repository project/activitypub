<?php

namespace Drupal\Tests\activitypub\Functional;

use Drupal\activitypub\Entity\ActivityPubActivityInterface;
use Drupal\Core\Url;

/**
 * Tests move functionality.
 *
 * @group activitypub
 */
class MoveTest extends ActivityPubTestBase {

  /**
   * Tests that a user follows a user on the new instance when this user moves.
   */
  public function testRemoteMove() {
    $assert_session = $this->assertSession();

    $this->enableActivityPub($assert_session);
    $this->drupalLogout();

    $storage = \Drupal::entityTypeManager()->getStorage('activitypub_activity');
    $actor_href_1 = Url::fromRoute('activitypub.user.self', ['user' => $this->authenticatedUserOne->id(), 'activitypub_actor' => $this->accountNameOne], ['absolute' => TRUE])->toString();
    $actor_href_2 = 'https://instance.one/users/follow-followed';
    $actor_href_3 = 'https://instance.one/users/follow-only';
    $this->followUserProgrammatically($this->authenticatedUserOne->id(), $actor_href_1, $actor_href_2);
    $this->followUserProgrammatically($this->authenticatedUserOne->id(), $actor_href_2, $actor_href_1, ActivityPubActivityInterface::INBOX, 'Accept');
    $this->followUserProgrammatically($this->authenticatedUserOne->id(), $actor_href_2, $actor_href_1, ActivityPubActivityInterface::INBOX, 'Follow', 'user');
    $this->followUserProgrammatically($this->authenticatedUserOne->id(), $actor_href_1, $actor_href_3);
    $this->followUserProgrammatically($this->authenticatedUserOne->id(), $actor_href_3, $actor_href_1, ActivityPubActivityInterface::INBOX, 'Accept', 'user');
    $this->clearQueue(ACTIVITYPUB_INBOX_QUEUE);
    $this->clearQueue(ACTIVITYPUB_OUTBOX_QUEUE);

    /** @var \Drupal\activitypub\Entity\ActivityPubActivityInterface[] $activities */
    $activities = $storage->loadMultiple();
    self::assertEquals(6, count($activities));
    self::assertEquals($activities[1]->getActor(), $actor_href_1);
    self::assertEquals($activities[1]->getObject(), $actor_href_2);
    self::assertEquals($activities[2]->getActor(), $actor_href_2);
    self::assertEquals($activities[2]->getObject(), $actor_href_1);

    $inbox = Url::fromRoute('activitypub.inbox', ['user' => $this->authenticatedUserOne->id(), 'activitypub_actor' => $this->accountNameOne])->toString();

    // Simulate incoming Move activities.
    $actor_href_2_new_instance = 'https://instance.two/users/follow-followed';
    $actor_href_3_new_instance = 'https://instance.two/users/follow-only';
    $payload = $this->getPayload($actor_href_2);
    $payload['id'] = $actor_href_2 . '#Move';
    $payload['type'] = "Move";
    $payload['object'] = $actor_href_2;
    $payload['target'] = $actor_href_2_new_instance;
    $this->sendInboxRequest($inbox, $payload);

    $payload['id'] = $actor_href_3 . '#Move';
    $payload['actor'] = $actor_href_3;
    $payload['object'] = $actor_href_3;
    $payload['target'] = $actor_href_3_new_instance;
    $this->sendInboxRequest($inbox, $payload);

    $storage->resetCache();
    $activities = $storage->loadMultiple();
    $count = \Drupal::queue(ACTIVITYPUB_OUTBOX_QUEUE)->numberOfItems();
    self::assertEquals(2, $count);

    // The first six activities don't exist anymore and 8 and 10 are the new
    // follow outbox request from user 1 to actor 2 and 3. The object in the
    // move activities is the new instance of the original actor.
    self::assertTrue(!isset($activities[1]));
    self::assertTrue(!isset($activities[2]));
    self::assertTrue(!isset($activities[3]));
    self::assertTrue(!isset($activities[4]));
    self::assertTrue(!isset($activities[5]));
    self::assertTrue(!isset($activities[6]));

    self::assertEquals($actor_href_2, $activities[7]->getActor());
    self::assertEquals($actor_href_2_new_instance, $activities[7]->getObject());
    self::assertEquals('Move', $activities[7]->getType());
    self::assertEquals($actor_href_1, $activities[8]->getActor());
    self::assertEquals($actor_href_2_new_instance, $activities[8]->getObject());
    self::assertEquals('Follow', $activities[8]->getType());
    self::assertEquals(ActivityPubActivityInterface::OUTBOX, $activities[8]->getCollection());

    self::assertEquals($actor_href_3, $activities[9]->getActor());
    self::assertEquals($actor_href_3_new_instance, $activities[9]->getObject());
    self::assertEquals('Move', $activities[9]->getType());
    self::assertEquals($actor_href_1, $activities[10]->getActor());
    self::assertEquals($actor_href_3_new_instance, $activities[10]->getObject());
    self::assertEquals('Follow', $activities[10]->getType());
    self::assertEquals(ActivityPubActivityInterface::OUTBOX, $activities[10]->getCollection());
  }

}