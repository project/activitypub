<?php

namespace Drupal\Tests\activitypub\Functional;

use Drupal\Core\Url;

/**
 * Tests that site is fetchable by other services.
 *
 * @group activitypub
 */
class FetchableTest extends ActivityPubTestBase {

  /**
   * Test that properly json is returned when using complex request headers.
   */
  public function testAcceptJsonWithProfile() {
    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    $this->enableActivityPub($assert_session);

    // Create type.
    $this->createType('Create', 'page', 'Note');

    // Add content.
    $this->drupalGet('node/add/page');
    $assert_session->statusCodeEquals(200);
    $assert_session->pageTextContains('ActivityPub outbox');

    $body = "Hello test";
    $edit = [
      'title[0][value]' => 'Page one',
      'body[0][value]' => $body,
      'activitypub_create' => 'map',
    ];

    $this->submitForm($edit, 'Save');
    $this->drupalLogout();

    // Comply with https://www.w3.org/TR/activitypub/#retrieving-objects
    // Tries to obtain the content as some remote sites do,
    $this->drupalGet('node/1', [], [
      'Accept' => 'application/activity+json, application/ld+json; profile="https://www.w3.org/ns/activitystreams", text/html;q=0.1',
    ]);
    $assert_session->responseHeaderContains('content-type', 'application/activity+json');

    $this->drupalGet('node/1', [], [
      'Accept' => 'application/activity+json',
    ]);
    $assert_session->responseHeaderContains('content-type', 'application/activity+json');
    $this->drupalGet('node/1', [], [
      'Accept' => 'application/ld+json; profile="https://www.w3.org/ns/activitystreams"',
    ]);
    $assert_session->responseHeaderContains('content-type', 'application/activity+json');

    // Obtain the alternate link when requesting html
    $this->drupalGet('node/1', [], ['Accept' => 'text/html']);

    // Get the head alternate link to obtain json.
    $alt_link_element = $this->xpath("//head/link[@rel='alternate' and @type='application/activity+json']");
    $this->assertEquals(1, count($alt_link_element));

    $alternate_link = $alt_link_element[0]->getAttribute('href');
    $this->drupalGet($alternate_link);
    $content = json_decode($page->getContent());

    // Be sure the obtained note through alternate links
    // has proper body.
    $this->assertEquals('<p>' . $body . '</p>', $content->content);

    $base_url = Url::fromRoute('<front>', [], ['absolute' => TRUE])->toString();

    // Be sure the obtained note through alternate links has proper id.
    $this->assertEquals($base_url . "node/1", $content->id);
  }

}