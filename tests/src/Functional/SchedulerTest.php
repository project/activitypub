<?php

namespace Drupal\Tests\activitypub\Functional;

use Drupal\Core\Url;
use Drupal\node\Entity\NodeType;
use Drupal\Tests\Traits\Core\CronRunTrait;

/**
 * Tests Scheduler integration.
 *
 * @group activitypub
 */
class SchedulerTest extends ActivityPubTestBase {

  use CronRunTrait;

  /**
   * The request time stored as integer for direct re-use in many tests.
   *
   * @var int
   */
  protected $requestTime;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'scheduler',
  ];

  /**
   * The authenticated user permissions.
   *
   * @var array
   */
  protected $authenticatedUserPermissions = [
    'allow users to enable activitypub',
    'administer nodes',
    'bypass node access',
    'access user profiles',
    'view own unpublished content',
    'schedule publishing of nodes',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create type.
    $this->createType('Create', 'page', 'Note');

    $nodeType = NodeType::load('page');

    // Enable scheduler on page bundle.
    // Add scheduler functionality to the content type.
    $nodeType->setThirdPartySetting('scheduler', 'publish_enable', TRUE)
      ->setThirdPartySetting('scheduler', 'unpublish_enable', TRUE)
      ->save();

    // Enable the scheduler fields in the default form display, mimicking what
    // would be done if the entity bundle had been enabled via admin UI.
    $this->container->get('entity_display.repository')
      ->getFormDisplay('node', $nodeType->id())
      ->setComponent('publish_on', ['type' => 'datetime_timestamp_no_default'])
      ->setComponent('unpublish_on', ['type' => 'datetime_timestamp_no_default'])
      ->save();

    $this->requestTime = $this->container->get('datetime.time')->getRequestTime();
  }

  /**
   * Tests scheduler integration.
   */
  public function testSchedulerIntegration() {
    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    /** @var \Drupal\activitypub\Entity\Storage\ActivityPubActorStorageInterface $activityStorage */
    $activityStorage = \Drupal::entityTypeManager()->getStorage('activitypub_activity');
    /** @var \Drupal\node\NodeStorageInterface $nodeStorage */
    $nodeStorage = \Drupal::entityTypeManager()->getStorage('node');

    $this->enableActivityPub($assert_session);

    $outbox = Url::fromRoute('activitypub.outbox', ['user' => $this->authenticatedUserOne->id(), 'activitypub_actor' => $this->accountNameOne], ['absolute' => TRUE])->toString();

    $this->drupalGet('node/add/page');
    $edit = [
      'title[0][value]' => 'Page one',
      'body[0][value]' => 'Hello world!',
      'activitypub_create' => 'map',
      'publish_on[0][value][date]' => \Drupal::service('date.formatter')->format(time() + 3600, 'custom', 'Y-m-d'),
      'publish_on[0][value][time]' => \Drupal::service('date.formatter')->format(time() + 3600, 'custom', 'H:i:s'),
      'unpublish_on[0][value][date]' => \Drupal::service('date.formatter')->format(time() + 7200, 'custom', 'Y-m-d'),
      'unpublish_on[0][value][time]' => \Drupal::service('date.formatter')->format(time() + 7200, 'custom', 'H:i:s'),
    ];
    $this->submitForm($edit, 'Save');

    // Check outbox, should be 0.
    $this->checkOutbox($outbox, $page, $assert_session, 0);

    $count = \Drupal::queue(ACTIVITYPUB_OUTBOX_QUEUE)->numberOfItems();
    self::assertEquals(0, $count);

    // We should have an unpublished activity and node.
    /** @var \Drupal\node\NodeInterface $node */
    $node = $nodeStorage->load(1);
    /** @var \Drupal\activitypub\Entity\ActivityPubActivityInterface $activity */
    $activity = $activityStorage->load(1);
    self::assertTrue(!is_null($activity));
    self::assertTrue(!$activity->isPublished());
    self::assertTrue(!$node->isPublished());

    // Run scheduler to publish.
    $node->set('publish_on', $this->requestTime - 7200);
    $node->save();
    $this->cronRun();

    // Check node is published.
    $nodeStorage->resetCache([1]);
    $node = $nodeStorage->load(1);
    self::assertTrue($node->isPublished());

    $count = \Drupal::queue(ACTIVITYPUB_OUTBOX_QUEUE)->numberOfItems();
    self::assertEquals(1, $count);

    // Check that activity is published.
    $activityStorage->resetCache([1]);
    $activity = $activityStorage->load(1);
    self::assertTrue($activity->isPublished());

    // Check node element.
    $this->drupalGet('node/1/edit');
    $assert_session->responseNotContains('activitypub_create');
    $assert_session->responseContains('activitypub_update');

    // Check outbox, it should be available now.
    $this->checkOutbox($outbox, $page, $assert_session, 1);

    // Run scheduler to unpublish.
    $node->set('unpublish_on', $this->requestTime - 7200);
    $node->save();
    $this->cronRun();

    // Check that node and activity are unpublished again.
    $nodeStorage->resetCache([1]);
    $activityStorage->resetCache([1]);
    $node = $nodeStorage->load(1);
    $activity = $activityStorage->load(1);
    self::assertTrue(!$activity->isPublished());
    self::assertTrue(!$node->isPublished());

    // Check outbox, it should be gone again.
    $this->checkOutbox($outbox, $page, $assert_session, 0);
  }

}
