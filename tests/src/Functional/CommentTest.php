<?php

namespace Drupal\Tests\activitypub\Functional;

use Drupal\Core\Url;
use Drupal\user\Entity\Role;
use Drupal\user\RoleInterface;

/**
 * Tests comment functionality.
 *
 * @group activitypub
 */
class CommentTest extends ActivityPubTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'comment',
    'field_ui',
  ];

  /**
   * The authenticated user permissions.
   *
   * @var array
   */
  protected $authenticatedUserPermissions = [
    'allow users to enable activitypub',
    'administer nodes',
    'bypass node access',
    'access user profiles',
    'access comments',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void{
    parent::setUp();

    /** @var \Drupal\user\RoleInterface $role */
    $role = Role::load(RoleInterface::ANONYMOUS_ID);
    $this->grantPermissions($role, ['access comments', 'post comments']);
  }

  /**
   * Test comment request.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testComment() {
    $assert_session = $this->assertSession();

    $this->enableActivityPub($assert_session, TRUE);
    $this->setTypeStatus('inbox_reply');
    $this->drupalLogout();
    $this->drupalLogin($this->adminUser);
    $edit = [
      'process_inbox_handler' => 'drush',
    ];
    $this->drupalGet('admin/config/services/activitypub');
    $this->submitForm($edit, 'Save configuration');
    $edit = [];
    $this->drupalGet('admin/structure/comment/manage/comment/fields/reuse');
    $this->submitForm($edit, 'Re-use');
    $this->submitForm([], 'Save settings');
    $this->drupalGet('admin/structure/comment/manage/comment/display');
    $this->submitForm(['fields[activitypub_activity][region]' => 'hidden'], 'Save');
    drupal_flush_all_caches();

    $node_content = 'But who will read it?';
    $node_values = [
      'type' => 'article',
      'uid' => $this->authenticatedUserTwo->id(),
      'title' => 'This is my first post',
      'body' => ['value' => $node_content],
    ];
    $node = $this->drupalCreateNode($node_values);
    $this->drupalLogout();

    // Actor href.
    $actor_href = Url::fromRoute('activitypub.user.self', ['user' => $this->authenticatedUserOne->id(), 'activitypub_actor' => $this->accountNameOne], ['absolute' => TRUE])->toString();

    $payload = [
      'type' => 'Create',
      '@context' => 'https://www.w3.org/ns/activitystreams',
      'id' => $actor_href . '/reply/1',
      'actor' => $actor_href,
      'to' => ['https://www.w3.org/ns/activitystreams#Public'],
      'object' => [
        'type' => 'Note',
        'to' => ['https://www.w3.org/ns/activitystreams#Public'],
        'attributedTo' => $actor_href,
        'id' => $actor_href . '/reply/1',
        'published' => '2020-03-12T09:27:03Z',
        'content' => 'I just fond it!',
        'inReplyTo' => $node->toUrl('canonical', ['absolute' => TRUE])->toString()
      ],
    ];
    $inbox = Url::fromRoute('activitypub.inbox', ['user' => $this->authenticatedUserTwo->id(), 'activitypub_actor' => $this->accountNameTwo])->toString();
    $response = $this->sendInboxRequest($inbox, $payload);
    self::assertEquals(202, $response->getStatusCode());

    /** @var \Drupal\activitypub\Entity\Storage\ActivityPubActorStorageInterface $activityStorage */
    $activityStorage = \Drupal::entityTypeManager()->getStorage('activitypub_activity');
    /** @var \Drupal\activitypub\Entity\ActivityPubActivityInterface $activity */
    $activity = $activityStorage->load(1);
    self::assertEquals("Create", $activity->getType());
    self::assertEquals($payload['id'], $activity->getExternalId());
    self::assertEquals($actor_href, $activity->getActor());
    self::assertEquals($payload['object']['id'], $activity->getObject());
    self::assertEquals($payload['object']['inReplyTo'], $activity->getReply());

    $count = \Drupal::queue(ACTIVITYPUB_INBOX_QUEUE)->numberOfItems();
    self::assertEquals(1, $count);

    $this->runInboxQueue();

    self::assertEquals($node->id(), $activity->getTargetEntityId());
    self::assertEquals($node->getEntityTypeId(), $activity->getTargetEntityTypeId());
    $this->drupalGet('node/1');
    $assert_session->responseContains($node_content);
    $assert_session->responseContains('I just fond it!');
    $assert_session->responseNotContains('Activity reference');

    /** @var \Drupal\comment\CommentStorageInterface $commentStorage */
    $commentStorage = \Drupal::entityTypeManager()->getStorage('comment');
    /** @var \Drupal\comment\CommentInterface $comment */
    $comment = $commentStorage->load(1);
    self::assertEquals($payload['object']['content'], $comment->get('comment_body')->value);
    self::assertTrue($comment->isPublished());
    self::assertEquals($activity->id(), $comment->get('activitypub_activity')->target_id);
    $assert_session->responseContains($payload['object']['content']);

    // Create another comment, which we'll keep eventually.
    $payload['id'] = $actor_href . '/reply/2';
    $payload['object']['id'] = $actor_href . '/reply/2';
    $payload['object']['content'] = 'I just found it!';
    $this->sendInboxRequest($inbox, $payload);
    $this->runInboxQueue();
    $activity2 = $activityStorage->load(2);
    $comment2 = $commentStorage->load(2);
    self::assertEquals($payload['object']['id'], $activity2->getObject());
    self::assertEquals($payload['object']['content'], $comment2->get('comment_body')->value);
    self::assertEquals($activity2->id(), $comment2->get('activitypub_activity')->target_id);
    $this->drupalGet('node/1');
    $assert_session->responseContains('I just fond it!');
    $assert_session->responseContains('I just found it!');

    // Send delete request for first comment.
    $delete_payload = [
      'id' => 'delete1',
      'type' => 'Delete',
      'actor' => $actor_href,
      'object' => [
        'id' => $actor_href . '/reply/1'
      ],
    ];
    $this->sendInboxRequest($inbox, $delete_payload);
    $activityStorage->resetCache([1, 2]);
    $activity = $activityStorage->load(1);
    self::assertNull($activity);
    $commentStorage->resetCache([1, 2]);
    $comment = $comment->load(1);
    self::assertNull($comment);
    $this->drupalGet('node/1');
    $assert_session->responseNotContains('I just fond it!');
    $assert_session->responseContains('I just found it!');
  }

}
