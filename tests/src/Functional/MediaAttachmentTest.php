<?php

namespace Drupal\Tests\activitypub\Functional;

use Drupal\Core\Url;

/**
 * Tests media attachment functionality.
 *
 * @group activitypub
 */
class MediaAttachmentTest extends ActivityPubTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'field_ui',
    'media',
  ];

  /**
   * The authenticated user permissions.
   *
   * @var array
   */
  protected $authenticatedUserPermissions = [
    'allow users to enable activitypub',
    'administer nodes',
    'bypass node access',
    'access user profiles',
    'view media',
    'create media',
  ];

  /**
   * Test media attachment.
   */
  public function testMediaAttachment() {
    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    $this->enableActivityPub($assert_session);
    $this->drupalLogout();

    $outbox = Url::fromRoute('activitypub.outbox', ['user' => $this->authenticatedUserOne->id(), 'activitypub_actor' => $this->accountNameOne,], ['absolute' => TRUE])->toString();

    $this->drupalLogin($this->authenticatedUserOne);

    // Add media.
    $this->drupalGet('/media/add/image');
    $page->fillField('Name', 'Foobar');
    $page->attachFileToField('Image', \Drupal::service('extension.list.module')->getPath('activitypub') . '/tests/fixtures/example.jpg');
    $page->pressButton('Save');

    // Create type.
    $mapping = ['field_media_image' => 'attachment'];
    $this->createType('Create', 'photo', 'Note', $mapping);

    // Add content.
    $this->drupalGet('node/add/photo');
    $assert_session->statusCodeEquals(200);
    $assert_session->pageTextContains('ActivityPub outbox');

    $edit = [
      'title[0][value]' => 'Photo one',
      'body[0][value]' => 'Hello media world!',
      'field_media_image[1]' => TRUE,
      'activitypub_create' => 'map',
    ];

    $this->submitForm($edit, 'Save');

    $photo = $this->getNodeByTitle('Photo one');

    $image_uri = $photo->field_media_image->entity->field_media_image->entity->getFileUri();
    $image_url = $this->container->get('file_url_generator')->generateAbsoluteString($image_uri);

    // Check that media element and node content are ok.
    self::assertEquals(1, $photo->field_media_image->target_id);

    $assert_session->addressEquals('node/1');
    $this->drupalGet('node/1/edit');

    $assert_session->responseNotContains('activitypub_create');
    $assert_session->responseContains('activitypub_update');
    $this->drupalLogout();

    $this->drupalGet($outbox);
    $content = json_decode($page->getContent());
    $this->drupalGet($content->first);
    $content = json_decode($page->getContent());
    $note = $content->orderedItems[0]->object;

    // Be sure the note attachment url is the same as media.
    self::assertEquals($image_url, $note->attachment[0]->url);
  }

}
