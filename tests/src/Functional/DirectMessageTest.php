<?php

namespace Drupal\Tests\activitypub\Functional;

use Drupal\activitypub\Entity\ActivityPubActivityInterface;
use Drupal\Core\Url;

/**
 * Tests Direct message functionality.
 *
 * @group activitypub
 */
class DirectMessageTest extends ActivityPubTestBase {

  /**
   * Test incoming and outgoing direct messages.
   */
  public function testDirectMessage() {
    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    /** @var \Drupal\activitypub\Entity\Storage\ActivityPubActorStorageInterface $storage */
    $storage = \Drupal::entityTypeManager()->getStorage('activitypub_activity');

    // Setup ActivityPub actors.
    $this->enableActivityPub($assert_session, TRUE);
    $this->drupalLogout();

    // Set outbox handler.
    $this->setOutboxHandler();

    $actor_href = Url::fromRoute('activitypub.user.self', ['user' => $this->authenticatedUserOne->id(), 'activitypub_actor' => $this->accountNameOne], ['absolute' => TRUE])->toString();
    $object_href = Url::fromRoute('activitypub.user.self', ['user' => $this->authenticatedUserTwo->id(), 'activitypub_actor' => $this->accountNameTwo], ['absolute' => TRUE])->toString();

    // Create type.
    $this->createType('Create', 'page', 'Note');

    // Send to outbox.
    $this->drupalLogin($this->authenticatedUserOne);
    $this->drupalGet('node/add/page');
    $edit = [
      'title[0][value]' => 'Hello!',
      'body[0][value]' => 'Hello, this is a direct message to you!',
      'activitypub_create' => 'map',
      'activitypub_to' => $object_href,
      'activitypub_visibility' => ActivityPubActivityInterface::VISIBILITY_PRIVATE,
      'status[value]' => FALSE,
    ];
    $this->submitForm($edit, 'Save');
    $this->drupalLogout();

    /** @var \Drupal\node\NodeInterface $node */
    $node = \Drupal::entityTypeManager()->getStorage('node')->load(1);

    $count = \Drupal::queue(ACTIVITYPUB_OUTBOX_QUEUE)->numberOfItems();
    self::assertEquals(1, $count);
    $this->runOutboxQueue();

    /** @var \Drupal\activitypub\Entity\ActivityPubActivityInterface $activity */
    $activity = $storage->load(1);
    $build = $activity->buildActivity();
    self::assertEquals([$object_href], $build['to']);
    self::assertEquals([$object_href], $build['object']['to']);
    self::assertEquals([], $build['cc']);
    self::assertEquals([], $build['object']['cc']);
    self::assertEquals(TRUE, $activity->isPrivate());
    $activity = $storage->load(2);
    $json = json_decode($activity->getPayLoad(), TRUE);
    self::assertEquals(TRUE, $activity->isPrivate());
    self::assertEquals([$object_href], $json['to']);
    self::assertEquals([$object_href], $json['object']['to']);
    self::assertEquals([], $json['cc']);
    self::assertEquals([], $json['object']['cc']);

    self::assertEquals($actor_href, $activity->getActor());
    self::assertEquals(ActivityPubActivityInterface::INBOX, $activity->getCollection());
    self::assertEquals('Create', $activity->getType());
    self::assertEquals($this->authenticatedUserTwo->id(), $activity->getOwnerId());
    self::assertEquals($node->toUrl('canonical', ['absolute' => TRUE])->toString(), $activity->getObject());
    self::assertEquals('Note', $json['object']['type']);
    self::assertEquals($json['id'], $activity->getExternalId());
    self::assertEquals($node->toUrl('canonical', ['absolute' => TRUE])->toString(), $json['object']['id']);
    self::assertEquals('<p>' . $node->get('body')->value . '</p>', $json['object']['content']);

    $this->drupalGet('nodeinfo/content');
    $content = json_decode($page->getContent());
    self::assertEquals(2, $content->usage->users->total);
    self::assertEquals(0, $content->usage->localPosts);

    $outbox = Url::fromRoute('activitypub.outbox', ['user' => $this->authenticatedUserOne->id(), 'activitypub_actor' => $this->accountNameOne], ['absolute' => TRUE])->toString();
    $this->drupalGet($outbox);
    $content = json_decode($page->getContent());
    self::assertEquals(0, $content->totalItems);
    $this->drupalGet($content->first);
    $assert_session->statusCodeEquals(200);
    $content = json_decode($page->getContent());
    self::assertEquals(0, count($content->orderedItems));

  }

}
