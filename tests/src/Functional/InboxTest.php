<?php

namespace Drupal\Tests\activitypub\Functional;

use Drupal\Core\Url;

/**
 * Tests inbox functionality.
 *
 * @group activitypub
 */
class InboxTest extends ActivityPubTestBase {

  /**
   * Tests receiving identical activities.
   */
  public function testReceivingIdenticalActivities() {
    $assert_session = $this->assertSession();

    $this->enableActivityPub($assert_session);
    $this->drupalLogout();

    // Inbox.
    $inbox = Url::fromRoute('activitypub.inbox', ['user' => $this->authenticatedUserOne->id(), 'activitypub_actor' => $this->accountNameOne])->toString();

    $payload = $this->getPayload();
    $response = $this->sendInboxRequest($inbox, $payload);
    self::assertEquals(202, $response->getStatusCode());

    /** @var \Drupal\activitypub\Entity\Storage\ActivityPubActorStorageInterface $activityStorage */
    $activityStorage = \Drupal::entityTypeManager()->getStorage('activitypub_activity');
    /** @var \Drupal\activitypub\Entity\ActivityPubActivityInterface $activity */
    $activity = $activityStorage->load(1);
    self::assertEquals($payload['id'], $activity->getExternalId());

    // Send the same payload, second activity should not exist.
    $response = $this->sendInboxRequest($inbox, $payload);
    self::assertEquals(202, $response->getStatusCode());
    $activity = $activityStorage->load(2);
    self::assertNull($activity);
  }

  /**
   * Test update activities.
   */
  public function testUpdateActivities() {
    $assert_session = $this->assertSession();
    $this->enableActivityPub($assert_session, TRUE);
    $this->drupalLogout();

    /** @var \Drupal\activitypub\Entity\Storage\ActivityPubActorStorageInterface $activityStorage */
    $activityStorage = \Drupal::entityTypeManager()->getStorage('activitypub_activity');

    $inbox = Url::fromRoute('activitypub.inbox', ['user' => $this->authenticatedUserOne->id(), 'activitypub_actor' => $this->accountNameOne])->toString();
    $actor_href_1 = Url::fromRoute('activitypub.user.self', ['user' => $this->authenticatedUserOne->id(), 'activitypub_actor' => $this->accountNameOne], ['absolute' => TRUE])->toString();
    $actor_href_2 = Url::fromRoute('activitypub.user.self', ['user' => $this->authenticatedUserTwo->id(), 'activitypub_actor' => $this->accountNameTwo], ['absolute' => TRUE])->toString();

    // Follow the user, so the followee check runs since we don't sign.
    $this->followUserProgrammatically($this->authenticatedUserOne->id(), $actor_href_1, $actor_href_2);

    $payload = $this->getPayload($actor_href_2);
    $this->sendInboxRequest($inbox, $payload);

    // Send update.
    $payload['object']['content'] = 'This is my updated message!';
    $payload['type'] = 'Update';
    $this->sendInboxRequest($inbox, $payload);

    /** @var \Drupal\activitypub\Entity\ActivityPubActivityInterface $activity */
    $activity = $activityStorage->load(2);
    self::assertTrue($activity->isPublished());
    /** @var \Drupal\activitypub\Entity\ActivityPubActivityInterface $activity_update */
    $activity_update = $activityStorage->load(3);
    self::assertNull($activity_update);
    $stored_payload = json_decode($activity->getPayLoad(), TRUE);
    self::assertEquals($stored_payload['object']['content'], $payload['object']['content']);
  }

}
