<?php

namespace Drupal\Tests\activitypub\Functional;

use Drupal\Core\Url;
use Drupal\views\Tests\ViewTestData;
use Drupal\views\ViewExecutable;

/**
 * Tests views functionality.
 *
 * @group activitypub
 */
class ViewsTest extends ActivityPubTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'views',
  ];

  /**
   * Views used by this test.
   *
   * @var array
   */
  public static $testViews = ['related_activities'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Ensure that the plugin definitions are cleared.
    foreach (ViewExecutable::getPluginTypes() as $plugin_type) {
      $this->container->get("plugin.manager.views.$plugin_type")->clearCachedDefinitions();
    }

    ViewTestData::createTestViews(get_class($this), ['activitypub_test']);
  }

  /**
   * Test relations.
   */
  public function testRelations() {
    $assert_session = $this->assertSession();

    // Setup ActivityPub actor.
    $this->enableActivityPub($assert_session, TRUE);
    $this->drupalLogout();

    // Set views block.
    $this->drupalPlaceBlock('views_block:related_activities-block_1');

    // Actor 2 url.
    $actor_href2 = Url::fromRoute('activitypub.user.self', ['user' => $this->authenticatedUserTwo->id(), 'activitypub_actor' => $this->accountNameTwo], ['absolute' => TRUE])->toString();

    /** @var \Drupal\activitypub\Entity\Storage\ActivityPubActorStorageInterface $storage */
    $storage = \Drupal::entityTypeManager()->getStorage('activitypub_activity');

    // Create page content type and content.
    $this->drupalCreateContentType(['type' => 'page']);
    $node = $this->drupalCreateNode(['type' => 'page', 'title' => 'Page 1', 'uid' => $this->authenticatedUserOne->id(), 'status' => 1]);
    $node_1_url = $node->toUrl('canonical', ['absolute' => TRUE])->toString();

    // Send inbox request as user 2 to user 1.
    $payload = [
      'type' => 'Like',
      '@context' => 'https://www.w3.org/ns/activitystreams',
      'id' => $actor_href2 . '/like/1',
      'actor' => $actor_href2,
      'to' => ['https://www.w3.org/ns/activitystreams#Public'],
      'object' => $node_1_url,
    ];

    $inbox = Url::fromRoute('activitypub.inbox', ['user' => $this->authenticatedUserOne->id(), 'activitypub_actor' => $this->accountNameOne])->toString();
    $response = $this->sendInboxRequest($inbox, $payload);
    self::assertEquals(202, $response->getStatusCode());

    // Verify entity id and entity type are set on this activity, pointing
    // to node 1.
    /** @var \Drupal\activitypub\Entity\ActivityPubActivityInterface $activity */
    $activity = $storage->load(1);
    self::assertEquals('Like', $activity->getType());
    self::assertEquals($node_1_url, $activity->getObject());
    self::assertEquals($node->id(), $activity->getTargetEntityId());
    self::assertEquals($node->getEntityTypeId(), $activity->getTargetEntityTypeId());

    // Verify the block is shown on node 1.
    $this->drupalGet('node/' . $node->id());
    $assert_session->responseContains('Related activities');
    $assert_session->responseNotContains('Object: ' . $node_1_url);
    $assert_session->responseNotContains('No related activities found.');

    // Verify the block is not shown on node 2.
    $node_2 = $this->drupalCreateNode(['type' => 'page', 'title' => 'Page 2', 'uid' => $this->authenticatedUserOne->id(), 'status' => 1]);
    $this->drupalGet('node/' . $node_2->id());
    $assert_session->responseContains('No related activities found.');
  }

}