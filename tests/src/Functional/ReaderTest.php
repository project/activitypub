<?php

namespace Drupal\Tests\activitypub\Functional;

use Drupal\activitypub\Entity\ActivityPubActivityInterface;
use Drupal\Core\Url;

/**
 * Tests reader functionality.
 *
 * @group activitypub
 */
class ReaderTest extends ActivityPubTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'reader',
    'field_ui',
    'link'
  ];

  /**
   * The authenticated user permissions.
   *
   * @var array
   */
  protected $authenticatedUserPermissions = [
    'allow users to enable activitypub',
    'administer nodes',
    'bypass node access',
    'access user profiles',
    'access reader'
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    \Drupal::configFactory()->getEditable('reader.settings')->set('filter_format', 'restricted_html')->save();

    // Install reader theme.
    /** @var \Drupal\Core\Extension\ThemeInstallerInterface $theme_installer */
    $theme_installer = \Drupal::service('theme_installer');
    $theme_installer->install(['reader_theme']);

    $this->setOutboxHandler();
    $this->setInboxHandler();
    $this->setTypeStatus('context');

    // Create page type.
    $this->createType('Create', 'page', 'Note', [], 'article');

    // Create reply (content) type.
    $mapping = ['field_link' => 'inReplyTo'];
    $this->createType('Create', 'reply', 'Note', $mapping, 'reply');
    $this->drupalAddLinkField('reply');
    drupal_flush_all_caches();
  }

  /**
   * Tests reader.
   */
  public function testReader() {
    $assert_session = $this->assertSession();

    // Setup ActivityPub actor.
    $this->enableActivityPub($assert_session, 2);
    $this->drupalLogout();

    $actor_href_1 = Url::fromRoute('activitypub.user.self', ['user' => $this->authenticatedUserOne->id(), 'activitypub_actor' => $this->accountNameOne], ['absolute' => TRUE])->toString();
    $actor_href_2 = Url::fromRoute('activitypub.user.self', ['user' => $this->authenticatedUserTwo->id(), 'activitypub_actor' => $this->accountNameTwo], ['absolute' => TRUE])->toString();

    $this->followUserProgrammatically($this->authenticatedUserOne->id(), $actor_href_1, ACTIVITYPUB_TEST_USER);
    $this->followUserProgrammatically($this->authenticatedUserOne->id(), $actor_href_1, $actor_href_2);
    $this->followUserProgrammatically($this->authenticatedUserOne->id(), $actor_href_2, $actor_href_1, ActivityPubActivityInterface::INBOX, 'Follow', 'user');

    // Create node.
    $this->drupalLogin($this->authenticatedUserOne);
    $this->drupalGet('node/add/page');
    $edit = [
      'title[0][value]' => 'Hello Awesome content!',
      'body[0][value]' => 'The best page in the world!',
      'activitypub_create' => 'article',
      'status[value]' => TRUE,
    ];
    $this->submitForm($edit, 'Save');
    $node = \Drupal::entityTypeManager()->getStorage('node')->load(1);
    $node_url = $node->toUrl('canonical', ['absolute' => TRUE])->toString();
    $this->drupalLogout();
    $this->clearQueue(ACTIVITYPUB_OUTBOX_QUEUE);

    // Create some outgoing content from user 2.
    $this->drupalLogin($this->authenticatedUserTwo);
    $this->drupalGet('node/add/page');
    $edit = [
      'title[0][value]' => '1',
      'activitypub_create' => 'article',
      'body[0][value]' => 'My first message',
      'activitypub_to' => $actor_href_1,
      'status[value]' => TRUE,
    ];
    $this->submitForm($edit, 'Save');
    $this->drupalGet('node/add/reply');
    $edit = [
      'title[0][value]' => 'A reply',
      'activitypub_create' => 'reply',
      'body[0][value]' => 'It sure is!',
      'activitypub_to' => $actor_href_1,
      'field_link[0][uri]' => $node_url,
      'status[value]' => TRUE,
    ];
    $this->submitForm($edit, 'Save');
    $this->drupalLogout();

    $count = \Drupal::queue(ACTIVITYPUB_OUTBOX_QUEUE)->numberOfItems();
    self::assertEquals(2, $count);
    $this->runOutboxQueue();

    $count = \Drupal::queue(ACTIVITYPUB_INBOX_QUEUE)->numberOfItems();
    self::assertEquals(1, $count);
    $this->runInboxQueue();

    // View timeline(s).
    $this->drupalLogin($this->authenticatedUserOne);
    $this->drupalGet('reader/sources/activitypub');
    $assert_session->responseContains($actor_href_2);
    $this->drupalGet('reader/timeline/activitypub/home');
    $assert_session->responseContains('Home (2)');
    $assert_session->responseContains('Notifications (1)');
    $assert_session->pageTextContains('In reply to ' . $node_url);
    $assert_session->responseContains('My first message');
    $assert_session->responseContains('It sure is!');
    $assert_session->responseContains($node->get('body')->value);
    $this->drupalGet('reader/timeline/activitypub/notifications');
    $assert_session->responseContains('This person is now following you!');
    $this->drupalGet('reader/search', ['query' => ['module' => 'activitypub', 'keyword' => 'message']]);
    $assert_session->responseContains('My first message');
    $assert_session->responseNotContains('It sure is!');

    // Mute testing.
    $this->drupalGet('reader/sources/activitypub');
    $this->clickLink('Mute');
    $this->drupalGet('reader/timeline/activitypub/home');
    $assert_session->responseNotContains('Home (2)');
    $assert_session->responseContains('Notifications (1)');
    $assert_session->pageTextNotContains('In reply to ' . $node_url);
    $assert_session->responseNotContains('My first message');
    $this->drupalGet('reader/timeline/activitypub/local');
    $assert_session->pageTextContains('In reply to ' . $node_url);
    $assert_session->responseContains('My first message');
    $this->drupalGet('reader/sources/activitypub');
    $this->clickLink('Unmute');
    $this->drupalGet('reader/timeline/activitypub/home');
    $assert_session->responseContains('Home (2)');
    $assert_session->responseContains('My first message');

    // Followers to goes into public.
    $this->drupalLogout();
    $this->drupalLogin($this->authenticatedUserTwo);
    $this->drupalGet('node/add/page');
    $edit = [
      'title[0][value]' => '2',
      'activitypub_create' => 'article',
      'body[0][value]' => 'For my followers only',
      'activitypub_visibility' => ActivityPubActivityInterface::VISIBILITY_FOLLOWERS,
      'status[value]' => TRUE,
    ];
    $this->submitForm($edit, 'Save');
    $this->drupalLogout();
    $count = \Drupal::queue(ACTIVITYPUB_OUTBOX_QUEUE)->numberOfItems();
    self::assertEquals(1, $count);
    $this->runOutboxQueue();
    $this->drupalLogin($this->authenticatedUserOne);
    $this->drupalGet('reader/timeline/activitypub/local');
    $assert_session->responseNotContains('For my followers only');
    $this->drupalGet('reader/timeline/activitypub/home');
    $assert_session->responseContains('For my followers only');
    $assert_session->responseNotContains('Direct messages (1)');
    $assert_session->responseContains('Home (3)');
  }

}