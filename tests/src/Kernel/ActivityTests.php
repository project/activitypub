<?php

namespace Drupal\Tests\activitypub\Kernel;

use Drupal\activitypub\Entity\ActivityPubActivityInterface;
use Drupal\Core\Url;
use Drupal\KernelTests\KernelTestBase;

/**
 * Tests activities.
 *
 * @group activitypub
 */
class ActivityTests extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'user',
    'activitypub',
    'activitypub_test',
    'image',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('user');
    $this->installEntitySchema('activitypub_activity');
    $this->installEntitySchema('activitypub_actor');
    $this->installConfig(['activitypub', 'activitypub_test']);
  }

  /**
   * Tests audience.
   */
  public function testAudience() {
    $account = \Drupal::entityTypeManager()->getStorage('user')->create(['mail' => 'test@cample.com', 'name' => 'User']);
    $account->save();

    $actor_values = ['entity_id' => $account->id(), 'type' => 'person', 'status' => 1, 'name' => 'Test'];
    $actor = \Drupal::entityTypeManager()->getStorage('activitypub_actor')->create($actor_values);
    $actor->save();
    $followers_url = Url::fromRoute('activitypub.followers', ['user' => $actor->getOwnerId(), 'activitypub_actor' => $actor->getName()], ['absolute' => TRUE])->toString();

    $values = [
      'uid' => $account->id(),
      'type' => 'Create',
      'config_id' => 'test_type',
      'collection' => ActivityPubActivityInterface::OUTBOX,
      'visibility' => ActivityPubActivityInterface::VISIBILITY_PUBLIC,
    ];

    /** @var \Drupal\activitypub\Entity\ActivityPubActivityInterface $activity */
    $activity = \Drupal::entityTypeManager()->getStorage('activitypub_activity')->create($values);

    // Public.
    $build = $activity->buildActivity();
    self::assertEquals([ActivityPubActivityInterface::PUBLIC_URL], $build['to']);
    self::assertEquals([$followers_url], $build['cc']);

    // Followers.
    $activity->setFollowers();
    $build = $activity->buildActivity();
    self::assertEquals([$followers_url], $build['to']);
    self::assertEquals([], $build['cc']);

    // Unlisted.
    $activity->setUnlisted();
    $build = $activity->buildActivity();
    self::assertEquals([], $build['to']);
    self::assertEquals([], $build['cc']);

    // Private.
    $activity->setPrivate();
    $activity->setTo(ACTIVITYPUB_TEST_USER);
    $build = $activity->buildActivity();
    self::assertEquals([ACTIVITYPUB_TEST_USER], $build['to']);
    self::assertEquals([], $build['cc']);
    self::assertEquals(ACTIVITYPUB_TEST_USER, $build['object']['tag'][0]->href);
    self::assertEquals('@random@example.com', $build['object']['tag'][0]->name);
  }

  /**
   * Tests removing old activities.
   */
  public function testRemoveOldActivities() {

    /** @var \Drupal\activitypub\Entity\Storage\ActivityPubActivityStorageInterface $storage */
    $storage = \Drupal::entityTypeManager()->getStorage('activitypub_activity');

    /** @var \Drupal\activitypub\Services\ActivityPubProcessClientInterface $client */
    $client = \Drupal::service('activitypub.process.client');

    $config = \Drupal::configFactory()->getEditable('activitypub.settings');

    // Create a bunch of activities.
    // Note: Follow inbox generates an outbox Accept automatically.
    // We also skip the Follow and outbox.
    $types = [
      'Create', 'Like', 'Announce', 'Follow',
    ];

    foreach ($types as $type) {
      $amount = 5;
      $days = 4;
      while ($amount != 0) {
        $time = strtotime("today - $days days");
        foreach ([ActivityPubActivityInterface::INBOX, ActivityPubActivityInterface::OUTBOX] as $collection) {

          // Don't create outbox Follow activities.
          if ($collection == ActivityPubActivityInterface::OUTBOX && $type == 'Follow') {
            continue;
          }

          $values = [
            'uid' => 1,
            'type' => $type,
            'external_id' => $this->randomMachineName(20),
            'actor' => $this->randomMachineName(20),
            'collection' => $collection,
            'visibility' => ActivityPubActivityInterface::VISIBILITY_PUBLIC,
          ];

          $activity = $storage->create($values);
          $activity->set('created', $time);
          $activity->save();

          // Create 'notifications' too.
          if ($collection == ActivityPubActivityInterface::INBOX && in_array($type, ['Like', 'Announce']) && $days > 12) {
            $activity = $storage->create($values);
            $activity->set('external_id', $this->randomMachineName(20));
            $activity->set('entity_id', 10);
            $activity->set('created', $time);
            $activity->save();
          }

        }
        $amount--;
        $days += 4;
      }
    }

    $count = $storage->getActivityCount();
    self::assertEquals(44, $count);
    $client->removeOldActivities();
    $count = $storage->getActivityCount();
    self::assertEquals(44, $count);
    $config->set('inbox_remove_x_days', 17)->save();
    $client->removeOldActivities();
    $count = $storage->getActivityCount();
    self::assertEquals(41, $count);
    $config->set('inbox_remove_x_days', 11)->save();
    $client->removeOldActivities();
    $count = $storage->getActivityCount();
    self::assertEquals(35, $count);

  }

}